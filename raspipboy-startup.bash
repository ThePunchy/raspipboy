#!/bin/bash
#
# Start raspipboy on the pitft screen
#
# To make the raspipboy run at boot, add this line to /etc/profile:
# /home/pi/raspipboy/raspipboy-startup.bash
#
cd /home/pi/raspipboy
SDL_FBDEV=/dev/fb1 python main.py
#SDL_MOUSEDRV=TSLIB SDL_VIDEODRIVER=fbcon SDL_MOUSEDEV=/dev/input/touchscreen SDL_FBDEV=/dev/fb1 python main.py
