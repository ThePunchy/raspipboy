"""An attempt to make a window with an attached scrollbar as generically as possible.
A skin set can be created to customize the look of your window.

Free for all purposes. No warranty expressed or implied.
-Sean "Mekire" McKiernan"""
import pygame as pg
import sys, os
from . import setup
from .subrect import Subrect
from lib.textwrapping import wrap_multi_line
from math import trunc


class RenderedListItem:
    def __init__(self, text, font, width, text_color=(255, 255, 255),
                 sel_box_color=(50, 50, 50),
                 sel_border_color=(255, 255, 255),
                 sel_border_width=2,
                 offset=0,
                 amount=None,
                 amount_spacer=5):
        self.text = text
        self.amount = amount
        self.amount_spacer = 5
        self.rendered_amount = None
        text_space = width - offset - 2*sel_border_width
        if self.amount is not None:
            rendered_amount = font.render(self.amount, 1, text_color)
            text_space -= rendered_amount.get_rect().width + amount_spacer

        self.lines = wrap_multi_line(text, font, text_space)

        self.line_count = len(self.lines)
        self.font = font
        self.offset = offset
        # Prerender lines
        rendered_lines = [font.render(line, 1, text_color) for line in self.lines]

        self.width = width
        self.height = sum(item.get_height() for item in rendered_lines) + 2*sel_border_width

        self.rendered_lines = pg.Surface((
            self.width, self.height
        ))
        y = sel_border_width
        for line in rendered_lines:
            self.rendered_lines.blit(line, (self.offset+sel_border_width, y))
            y += line.get_height()
        if self.amount is not None:
            self.rendered_lines.blit(rendered_amount,
                         (width - rendered_amount.get_rect().width - sel_border_width, sel_border_width))

        self.sel_box = pg.Surface((
            self.width, self.height
        ))
        self.sel_box.fill(sel_box_color)
        pg.draw.rect(self.sel_box, sel_border_color, self.sel_box.get_rect(), sel_border_width)

    def draw(self, surface, pos, selected=False):
        """Draw lines and selection box on the given surface"""
        if selected:
            surface.blit(self.sel_box, pos)
        surface.blit(self.rendered_lines, pos, special_flags=pg.BLEND_ADD)


class ScrollWindow(object):
    def __init__(self, subrect, content, font, **kwargs):
        """Create a scrollbar window with location and size defined by
        rect (x,y,width,height). Content is the list of strings to display
        in the scroll-window.  Various key-word arguments can also be used
        to further customize the menu (see process_kwargs for the complete list)."""
        if setup.GFX == None:
            setup.GFX = setup.load_default_images(setup.DEFAULT_RESOURCES)
        self.subrect = subrect
        self.content = content
        self.font = font
        self.__highlight_index = 0
        self.__index = 0
        self.process_kwargs(kwargs)
        self.render_content()
        self.set_bg_image()
        self.gen_bar_details()
        self.changed = True

    @property
    def index(self):
        return self.__index

    @index.setter
    def index(self, index):
        if index != self.__index:
            self.changed = True
            self.__index = index

    @property
    def highlight_index(self):
        return self.__highlight_index

    @highlight_index.setter
    def highlight_index(self, highlight_index):
        if highlight_index != self.__highlight_index:
            self.changed = True
            self.__highlight_index = highlight_index

    def process_kwargs(self, kwargs):
        """All of the following can be used as keyword arguments.  Any or all of
        them can be passed to the init by keyword, or a dict of them can be passed
        via the **dict keyword unpacking syntax."""
        self.kwargs = {"background": None,
                       "bg_color": (0, 0, 0),
                       "text_color": (255, 255, 255),
                       "sel_box_color": (0, 0, 0, 100),
                       "sel_border_color": (255, 255, 0),
                       "sel_border_width": 2,
                       "text_offset": 5,
                       "bar_bg_image": setup.GFX["bar_bg"],
                       "bar_button_images": [setup.GFX["button"].subsurface((0, 0, 12, 25)),
                                             setup.GFX["button"].subsurface((0, 0, 12, 25))],
                       "amounts" : None,
                       "amount_spacer" : 5,
                       "bar_slider_image": setup.GFX["bar"],
                       "bar_rect": None
                       }
        for kwarg in kwargs:
            if kwarg in self.kwargs:
                self.kwargs[kwarg] = kwargs[kwarg]
            else:
                raise AttributeError("Invalid keyword: {}".format(kwarg))

    def set_bg_image(self):
        """if an image has been suplied via keyword "background", use that image;
        otherwise fill the background surface with the background color.
        (background changable via keyword "bg_color")"""
        if not self.kwargs["background"]:
            self.kwargs["background"] = pg.Surface(self.subrect.size).convert_alpha()
            self.kwargs["background"].fill(self.kwargs["bg_color"])
            resize = setup.GFX["alph_grad"].copy()
            resize = pg.transform.smoothscale(resize, self.subrect.size)
            self.kwargs["background"].blit(resize, (0, 0))
        self.image = pg.Surface(self.subrect.size).convert()

    def render_content(self):
        """Prerender the menu content."""
        render_list = []

        for i in range(len(self.content)):
            item = self.content[i]
            amount = None
            if self.kwargs["amounts"] is not None:
                amount = self.kwargs["amounts"][i]
            render_list.append(RenderedListItem(item, self.font, self.subrect.width,
                                                self.kwargs["text_color"],
                                                self.kwargs['sel_box_color'],
                                                self.kwargs['sel_border_color'],
                                                self.kwargs['sel_border_width'],
                                                self.kwargs['text_offset'],
                                                amount,
                                                self.kwargs['amount_spacer']
                               ))
        self.rendered = render_list
        self.total_lines = sum(x.line_count for x in render_list)

    def activate_highlight(self):
        """Sets the highlight_index to the currently highlighted item.  If no
        items are currently highlighted, highlight_index is set to None."""
        if not self.Bar.drag:
            mouse = pg.mouse.get_pos()
            if self.subrect.rel2display().collidepoint(mouse) and not self.Bar.subrect.rel2display().collidepoint(
                    mouse):
                index = self.get_mouse_item_index()
                if index is not None:
                    self.highlight_index = index
                    self.capture_highlight()

    def get_mouse_item_index(self):
        mousey = self.subrect.rel2self(pg.mouse.get_pos())[1]
        y = 0
        for i in range(self.index, len(self.rendered)):
            if mousey < y + self.rendered[i].height:
                return i
            y += self.rendered[i].height
        return None

    def gen_bar_details(self):
        """Calculate some needed bar related variables and create a ScrollBar instance."""
        self.index = 0
        self.lines_per_page = trunc(self.subrect.height / (self.rendered[0].height / self.rendered[0].line_count))
        self.lines_before_item = [0]
        self.last_index = None
        for item in self.rendered:
            self.lines_before_item += [self.lines_before_item[-1] + item.line_count]
            if self.last_index is None and \
                                    self.lines_before_item[-1] + self.lines_per_page > self.total_lines:
                self.last_index = len(self.lines_before_item) - 1

        if self.kwargs["bar_button_images"]:
            but_size = self.kwargs["bar_button_images"][0].get_size()
        else:
            but_size = (self.rendered[0].get_height(),) * 2

        # Put Bar in area supplied by user, otherwise display it on the right side
        if self.kwargs['bar_rect'] is not None:
            bar_r = Subrect(self.kwargs['bar_rect'])
        else:
            bar_r = Subrect(
                (self.subrect.width - but_size[0], 0, but_size[0], self.subrect.height),
                self.subrect
            )
        self.Bar = _ScrollBar(bar_r, but_size,
                              self.lines_per_page, self.total_lines,
                              self.kwargs)

    def update_bar_rect(self):
        """Update location of bar_rect. If dragging find new index."""
        if self.Bar.drag:
            mouse_y = pg.mouse.get_pos()[1]
            line_diff = int((mouse_y - self.Bar.drag[0]) // self.Bar.per_line)
            i = self.Bar.drag[1]
            while line_diff < 0 and i > 0:
                i -= 1
                line_diff += self.rendered[i].line_count
            while line_diff > self.rendered[i].line_count and i < self.last_index:
                line_diff -= self.rendered[i].line_count
                i += 1
            self.index = i

        self.Bar.slider_rect.y = self.Bar.button_size[1] + (self.lines_before_item[self.index] * self.Bar.per_line)
        # if self.index == len(self.content)-self.lines_per_page:
        #     self.Bar.slider_rect.y = self.Bar.subrect.height-self.Bar.button_size[1]-self.Bar.slider_rect.height

    def update_content(self):
        """Blit the content highlighting if called for."""
        y = 0
        i = self.index
        while y < self.subrect.height and i < len(self.rendered):
            self.rendered[i].draw(self.image, (0,y), i == self.highlight_index)
            y += self.rendered[i].height
            i += 1

    def update(self, Surf):
        """Update entire window."""
        # self.activate_highlight()
        if self.Bar.drag or self.changed:
            self.update_bar_rect()
        if self.changed:
            self.image.blit(self.kwargs["background"], (0, 0))
            self.update_content()
            # Draw Bar on Tab if area is supplied by user
            if self.kwargs['bar_rect'] is not None:
                self.Bar.update(Surf)
            # otherwise draw inside the window
            else:
                self.Bar.update(self.image)
            Surf.blit(self.image, self.subrect)
            self.changed = False
            return True
        return False

    def capture_highlight(self):
        if self.highlight_index < self.index:
            self.index = self.highlight_index
        while self.lines_before_item[self.highlight_index + 1] - self.lines_before_item[self.index] > \
                self.lines_per_page:
            self.index += 1

    def get_events(self, event):
        """Process events for window.  This must be passed events from the main
        event loop of your program.  A selected content string will be returned if
        clicked while highlighted."""
        mouse = pg.mouse.get_pos()
        if isinstance(event, list):
            self.highlight_index -= event[2]
            if self.highlight_index < 0:
                self.highlight_index = 0
            if self.highlight_index >= len(self.content):
                self.highlight_index = len(self.content) - 1
            self.capture_highlight()
        elif event == 'sel':
            self.activate_highlight()
            if (self.Bar.subrect.rel2display().collidepoint(mouse)
                and len(self.content) > self.lines_per_page):
                if self.Bar.button_rects[0].rel2display().collidepoint(mouse):
                    # User clicks up arrow.
                    if self.index > 0:
                        self.index -= 1
                elif self.Bar.button_rects[1].rel2display().collidepoint(mouse):
                    # User clicks down arrow.
                    if self.index < self.last_index:
                        self.index += 1
                elif self.Bar.slider_rect.rel2display().collidepoint(mouse):
                    # User clicks slide bar.
                    self.Bar.drag = (pg.mouse.get_pos()[1], self.index)
                else:
                    if mouse[1] < self.Bar.slider_rect.rel2display().y:
                        # User clicks area above slide bar.
                        lines_before_index = self.lines_before_item[self.index]
                        while self.index > 0 and \
                              lines_before_index - self.lines_before_item[self.index] < self.lines_per_page:
                            self.index -= 1
                    elif mouse[1] > self.Bar.slider_rect.rel2display().bottom:
                        # User clicks area below slide bar.
                        lines_before_index = self.lines_before_item[self.index]
                        while self.index < self.last_index and \
                              self.lines_before_item[self.index] - lines_before_index < self.lines_per_page:
                            self.index += 1
        elif event == 'unsel':
            self.Bar.drag = False


class _ScrollBar:
    def __init__(self, subrect, button_size,
                 lines_per_page, total_lines,
                 kwarg_dict):
        """Contains details of target rects and how to draw the scroll bar itself."""
        self.subrect = subrect
        self.lines_per_page = lines_per_page
        self.total_lines = total_lines
        self.kwargs = kwarg_dict
        self.button_size = button_size
        self.button_rects = [Subrect(((0, 0), self.button_size), self.subrect),
                             Subrect(((0, self.subrect.height - self.button_size[1]), self.button_size), self.subrect)]
        self.barsize, self.per_line = self.make_bar()
        self.slider_rect = Subrect((0, self.button_size[1], self.button_size[0], self.barsize), self.subrect)
        self.drag = False

    def make_bar(self):
        """Calculate size of bar and the number of pixels between items in the list."""
        total_bar_height = self.subrect.height - 2 * self.button_size[1]
        if self.total_lines > self.lines_per_page:
            barsize = total_bar_height * (self.lines_per_page / float(self.total_lines))
            per_line = (total_bar_height - barsize) / (self.total_lines - self.lines_per_page + 1)
            if barsize < 10:
                barsize = 10
        else:
            barsize = total_bar_height
            per_line = 10
        return barsize, per_line

    def make_image(self):
        """Draw all elements of the scroll bar. Defaults are used if images
        have not been provided."""
        temp = pg.Surface(self.subrect.size).convert()
        if self.kwargs["bar_bg_image"]:
            resize = pg.transform.smoothscale(self.kwargs["bar_bg_image"], self.subrect.size)
            temp.blit(resize, (0, 0))
        else:
            temp.fill(0)
        if self.kwargs["bar_button_images"]:
            temp.blit(self.kwargs["bar_button_images"][0], self.button_rects[0])
            temp.blit(self.kwargs["bar_button_images"][1], self.button_rects[1])
        else:
            temp.fill((255, 0, 0), self.button_rects[0])
            temp.fill((255, 0, 0), self.button_rects[1])
        if self.kwargs["bar_slider_image"]:
            temp.blit(self.bar_from_image(), self.slider_rect)
        else:
            temp.fill((0, 0, 255), self.slider_rect)
            inner = self.slider_rect.inflate((-2, -2))
            temp.fill(-1, inner)
        self.image = temp

    def bar_from_image(self):
        """Creates a slider of the needed size based on a slider image.
        It is a little bit hacky and would probably need to be edited a bit
        for different slider images. Currently it fills the whole slider with
        a strip from the image at coordinates (0,4), then draws the center of
        the slider image, and finally draws the top and bottom two pixel strips
        at the top and bottom."""
        raw = self.kwargs["bar_slider_image"]
        raw_rect = raw.get_rect()
        center = raw.subsurface((0, 4, raw_rect.width, raw_rect.height - 8))
        filler = raw.subsurface((0, 4, raw_rect.width, 1))
        top = raw.subsurface((0, 0, raw_rect.width, 2))
        bottom = raw.subsurface((0, raw_rect.height - 2, raw_rect.width, 2))

        bar_img = pg.Surface(self.slider_rect.size).convert()
        image_center_rect = center.get_rect(center=bar_img.get_rect().center)
        for j in range(self.slider_rect.height):
            bar_img.blit(filler, (0, j))
        bar_img.blit(center, image_center_rect)  # middle of bar
        bar_img.blit(top, (0, 0))  # top of bar
        bar_img.blit(bottom, (0, self.slider_rect.height - 2))
        return bar_img

    def update(self, Surf):
        self.make_image()
        Surf.blit(self.image, self.subrect)
