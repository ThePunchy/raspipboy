# RasPipBoy: A Pip-Boy 3000 implementation for Raspberry Pi
#	Neal D Corbett, 2013
# Class for 'Stats' tab

import pygame, os, time, random, math, string, numpy
from pygame.locals import *
import config
import textrect
import pipboy_headFoot as headFoot
import sys
from guipack import scrollwindow, setup
from guipack.subrect import Subrect
from pprint import pprint
from pipboy_scrollmenu import create_scrollmenu

from config import cornerPadding, cornerHeight

class Tab_Stats:
    name = "STATS"

    # Status page includes further sub-pages...
    class Mode_Status:

        class Condition:

            changed = True
            pageListImg = None

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = 'CND'
                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

            def drawPage(self):
                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    # Draw status-boy to image:
                    self.pageCanvas.fill((0, 0, 0))
                    imageSize = config.HEIGHT
                    image = config.IMAGES["statusboy"]
                    image = pygame.transform.smoothscale(image, (imageSize, imageSize))
                    self.pageCanvas.blit(image, (((config.WIDTH - imageSize) / 2), 0), None, pygame.BLEND_ADD)

                    # Print player-name/level under statusboy:
                    text = ("%s - LEVEL %s" % (config.PLAYERNAME, config.PLAYERLEVEL))
                    textImg = config.FONT_LRG.render(text, True, config.DRAWCOLOUR, (0, 0, 0))
                    self.pageCanvas.blit(textImg, ((config.WIDTH - textImg.get_width()) / 2, config.HEIGHT * 0.805),
                                         None, pygame.BLEND_ADD)

                    # Add page-list:
                    self.pageCanvas.blit(self.pageListImg, (0, 0), None, pygame.BLEND_ADD)

                return self.pageCanvas, pageChanged

            # Called every view changes to this sub-page:
            def resetPage(self):
                True

            # Consume events passed to this sub-page:
            def ctrlEvents(self, events):
                True

        # Used for line-based stats:
        class StatLine:

            changed = True
            firstDraw = True

            # Default values:
            curVal = -1
            minVal = 0
            maxVal = 100

            setVal = maxVal
            frameNum = 0

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = args[1]
                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

            def updateStatus(self):

                newVal = -1

                if (self.name == 'BAT'):  # Show Battery status

                    newVal = self.setVal

                    if (config.USE_SERIAL):
                        # Only do this every so often...
                        if (self.frameNum == 0):
                            # Send query to Teensy to get current battery voltage:
                            self.rootParent.ser.write("volts\n")
                        # (value is returned and set via page-events queue)
                        elif (self.frameNum == 15):
                            self.frameNum = -1;
                        self.frameNum += 1;

                elif (self.name == 'TMP'):  # Show Temperature

                    newVal = self.setVal

                    if (config.USE_SERIAL):
                        # Only do this every so often...
                        if (self.frameNum == 0):
                            # Send query to Teensy to get current temperature:
                            self.rootParent.ser.write("temp\n")
                        # (value is returned and set via page-events queue)
                        elif (self.frameNum == 15):
                            self.frameNum = -1;
                        self.frameNum += 1;

                elif (self.name == 'WAN'):  # Show WiFi signal-level
                    newVal = 0
                    if (config.USE_INTERNET):
                        # Get relevant line from proc:
                        line = ""
                        try:
                            with open('/proc/net/wireless', 'r') as f:
                                f.readline()
                                f.readline()
                                line = f.readline()

                            # Get 'Quality:Level' value:
                            tokens = string.split(line)
                            token = tokens[3]
                            token = string.replace(token, ".", "")
                            newVal = string.atoi(token)
                        except:
                            pass
                else:
                    newVal = self.minVal

                if (newVal != self.curVal):
                    self.curVal = newVal
                    self.changed = True

            def drawPage(self):

                # Draw initial line-gauge:
                if (self.firstDraw):

                    self.firstDraw = False
                    img = self.pageListImg

                    cornerPadding = headFoot.cornerPadding
                    charWidth = config.charWidth
                    charHeight = config.charHeight

                    lineY = (config.HEIGHT * 0.66)
                    lineYDn = (lineY + (charHeight * 1.6))
                    lineDnAX = (config.WIDTH * 0.33)
                    lineRtX = (config.WIDTH - cornerPadding)

                    # Draw page-name under gauge:
                    textImg = config.FONT_LRG.render(self.name, True, config.DRAWCOLOUR, (0, 0, 0))
                    img.blit(textImg, (lineDnAX + 6, lineY + 4))

                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(cornerPadding, lineY), (lineDnAX, lineY), (lineDnAX, lineYDn)], 2)
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX + 6, lineY), (lineRtX, lineY), (lineRtX, lineYDn)], 2)

                    # Draw subdivision-lines:
                    divHeight = (charHeight * 0.6)
                    gaugeStartX = (config.WIDTH * 0.5)
                    gaugeEndX = (lineRtX - (divHeight * 2))

                    self.gaugeStartX = gaugeStartX
                    self.gaugeEndX = gaugeEndX

                    divWidth = (gaugeEndX - gaugeStartX) / 15
                    divPosX = gaugeStartX
                    divYA = (lineY + divHeight)
                    divYB = (lineY + (divHeight * 0.3))

                    # Set up values for drawing pointer-arrow later:
                    self.arrowTopY = (lineY + divHeight)
                    self.arrowHeadX = (divHeight * 0.65)
                    self.arrowHeadY = (self.arrowTopY + (self.arrowHeadX))
                    self.arrowBtmY = (self.arrowHeadY + (2 * divHeight))
                    self.arrowTextY = (self.arrowBtmY - (0.9 * config.charHeight))

                    for n in range(0, 5):
                        for n in range(0, 2):
                            divPosX += divWidth
                            pygame.draw.lines(img, config.DRAWCOLOUR, False, [(divPosX, lineY), (divPosX, divYB)], 2)

                        divPosX += divWidth
                        if (divPosX < self.gaugeEndX):
                            pygame.draw.lines(img, config.DRAWCOLOUR, False, [(divPosX, lineY), (divPosX, divYA)], 2)

                    # Draw range-end triangles:
                    pygame.draw.polygon(img, config.DRAWCOLOUR, [(gaugeStartX, lineY), (gaugeStartX, lineY + divHeight),
                                                                 (gaugeStartX - divHeight, lineY)], 0)
                    pygame.draw.polygon(img, config.DRAWCOLOUR, [(gaugeEndX, lineY), (gaugeEndX, lineY + divHeight),
                                                                 (gaugeEndX + divHeight, lineY)], 0)
                else:
                    # Only bother acquiring stats after first page-draw call...
                    self.updateStatus()

                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    self.pageCanvas = self.pageListImg.convert()

                    # Draw value arrow/text:
                    valXPos = int(
                        numpy.interp(self.curVal, [self.minVal, self.maxVal], [self.gaugeStartX, self.gaugeEndX]))

                    textImg = config.FONT_LRG.render(str(self.curVal), True, config.DRAWCOLOUR, (0, 0, 0))
                    textPosX = (valXPos - self.arrowHeadX - textImg.get_width() + 2)
                    self.pageCanvas.blit(textImg, (textPosX, self.arrowTextY))

                    pygame.draw.polygon(self.pageCanvas, config.DRAWCOLOUR,
                                        [(valXPos, self.arrowTopY), (valXPos - self.arrowHeadX, self.arrowHeadY),
                                         (valXPos + self.arrowHeadX, self.arrowHeadY)], 0)
                    valXPos -= 1
                    pygame.draw.lines(self.pageCanvas, config.DRAWCOLOUR, False,
                                      [(valXPos, self.arrowHeadY), (valXPos, self.arrowBtmY)], 2)

                return self.pageCanvas, pageChanged

            # Called every view changes to this sub-page:
            def resetPage(self):
                True

            # Consume events passed to this sub-page:
            def ctrlEvents(self, events):

                if (self.name == 'BAT'):  # Get battery-status events
                    for event in events:
                        if (type(event) is str) and (event.startswith('volts')):
                            print(event);  # DEBUG PRINT
                            tokens = string.split(event);

                            batVolts = float(tokens[1]);
                            minVolts = 6.30;
                            maxVolts = 7.68;
                            self.setVal = int(100 * ((batVolts - minVolts) / (maxVolts - minVolts)));
                elif (self.name == 'TMP'):  # Get temperature-status events
                    for event in events:
                        if (type(event) is str) and (event.startswith('temp')):
                            print(event);  # DEBUG PRINT
                            tokens = string.split(event);

                            tempVal = float(tokens[1]);
                            minTemp = -40.0;
                            maxTemp = 125.0;
                            self.setVal = tempVal;
                        # int(100 * ((tempVal - minTemp) / (maxTemp - minTemp)));

        ######################################

        changed = True
        subPageNum = 0
        curSubPage = None

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "Status"

            # Set up list of sub-pages:
            Condition = self.Condition
            StatLine = self.StatLine
            self.subPages = [Condition(self), StatLine(self, 'RAD'), StatLine(self, 'TMP'), StatLine(self, 'BAT'),
                             StatLine(self, 'WAN'), StatLine(self, 'GPS')]

            # Generate list-image for each sub-page:
            for thisPageNum in range(0, len(self.subPages)):

                thisPage = self.subPages[thisPageNum]
                thisPage.pageListImg = pygame.Surface((config.WIDTH, config.HEIGHT))

                TextX = (config.charWidth * 2.9)
                TextY = (config.charHeight * 3)

                # Draw page-names, with box around selected one
                for subPageNum in range(0, len(self.subPages)):

                    doSelBox = (subPageNum == thisPageNum)

                    BackColour = None
                    if doSelBox:
                        BoxColour = (config.SELBOXGREY, config.SELBOXGREY, config.SELBOXGREY)

                    thisText = self.subPages[subPageNum].name
                    # print thisText

                    textImg = config.FONT_LRG.render(thisText, True, config.DRAWCOLOUR, (0, 0, 0))

                    TextWidth = (textImg.get_width())
                    textPos = (TextX, TextY)

                    if (doSelBox):
                        TextRect = (TextX - 2, TextY - 2, TextWidth + 4, config.charHeight + 4)
                        pygame.draw.rect(thisPage.pageListImg, BoxColour, TextRect, 0)
                        TextRect = (TextRect[0] - 2, TextRect[1], TextRect[2] + 2, TextRect[3])
                        pygame.draw.rect(thisPage.pageListImg, config.DRAWCOLOUR, TextRect, 2)

                    thisPage.pageListImg.blit(textImg, textPos, None, pygame.BLEND_ADD)

                    TextY += (config.charHeight * 1.5)

                # Do initial page-draw, caching for later use:
                thisPage.drawPage()

        def drawPage(self):
            # Pass sub-page canvas up to tab-draw function...
            subPageCanvas, subPageChanged = self.curSubPage.drawPage()

            pageChanged = (self.changed or subPageChanged)
            self.changed = False

            return subPageCanvas, pageChanged

        # Called every time view changes to this page:
        def resetPage(self):
            self.subPageNum = 0
            self.curSubPage = self.subPages[self.subPageNum]

        # Consume events passed to this page:
        def ctrlEvents(self, events):

            for event in events:

                # Arrow-keys change subpage-number:
                if (type(event) is list):

                    listMove = event[2]
                    newPageNum = self.subPageNum - listMove

                    if (newPageNum < 0):
                        newPageNum = 0
                    elif (newPageNum >= len(self.subPages)):
                        newPageNum = (len(self.subPages) - 1)

                    if (newPageNum != self.subPageNum):
                        if config.USE_SOUND:
                            config.SOUNDS["changemode"].play()
                        self.subPageNum = newPageNum
                        self.curSubPage = self.subPages[newPageNum]
                        self.changed = True

            # Pass events to subpage too, just in case I've set them up as consumers too:
            self.curSubPage.ctrlEvents(events)

    class Mode_SPECIAL:

        # Used for line-based stats:
        class SpecLine:

            changed = True
            firstDraw = True

            frameNum = 0

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = args[1]
                self.pic = args[2]
                self.text = args[3]
                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

                #print(self.special)
                #print(self.special['Strength'])

            def drawPage(self):

                # Draw initial line-gauge:
                if (self.firstDraw):

                    self.firstDraw = False
                    img = self.pageListImg

                    # Assign current special picture to variable
                    currentPic = self.pic

                    # Center the image of the SPECIAL above the text for it
                    img.blit(currentPic, ((config.WIDTH * 0.43), (config.HEIGHT * 0.000000000001)))

                    cornerPadding = headFoot.cornerPadding
                    charWidth = config.charWidth
                    charHeight = config.charHeight

                    lineY = (config.HEIGHT * 0.66)
                    lineYDn = (lineY + (charHeight * 1.6))
                    lineDnAX = (config.WIDTH * 0.40)
                    lineRtX = (config.WIDTH - cornerPadding)

                    # Setup text box for word wrapping
                    textBoxWidth = (lineRtX - (lineDnAX + 6))
                    textBoxHight = (charHeight * 5)

                    # Draw text box wrapped text
                    my_font = config.FONT_MED
                    my_string = self.text
                    my_rect = pygame.Rect(lineDnAX + 6, lineY + 4, textBoxWidth, textBoxHight)
                    rendered_text = textrect.render_textrect(my_string, my_font, my_rect, config.DRAWCOLOUR, (0, 0, 0), 0)
                    if rendered_text:
                        img.blit(rendered_text, my_rect )

                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX + 6, lineY), (lineRtX, lineY), (lineRtX, lineYDn)], 2)

                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    self.pageCanvas = self.pageListImg.convert()

                return self.pageCanvas, pageChanged

            # Called every view changes to this sub-page:
            def resetPage(self):
                True

        ######################################

        changed = True
        subPageNum = 0
        curSubPage = None

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "S.P.E.C.I.A.L."
            self.counter = 0

            # Set up list of sub-pages:
            SpecLine = self.SpecLine

            ### TODO - Need to fix sorting so it spells SPECIAL.
            # Loop through parameters from file and create page entry in list for each.
            for key in sorted(config.specialDIC):
                # print(config.specialDIC[key]['name'])
                # print(config.specialDIC[key]['filepath'])
                # print(config.specialDIC[key]['text'])
                self.currentName = str(config.specialDIC[key]['name'])
                self.currentPath = pygame.image.load(config.specialDIC[key]['filepath'])
                self.currentTXT = str(config.specialDIC[key]['text'])
                if self.counter <= 0:
                    self.subPages = [SpecLine(self, self.currentName, self.currentPath, self.currentTXT)]
                else:
                    self.subPages.extend([SpecLine(self, self.currentName, self.currentPath, self.currentTXT)])
                self.counter += 1
                #print('Counter=', self.counter)

            # Generate list-image for each sub-page:
            for thisPageNum in range(0, len(self.subPages)):

                thisPage = self.subPages[thisPageNum]
                thisPage.pageListImg = pygame.Surface((config.WIDTH, config.HEIGHT))

                # Set position for SPECIAL attribute text
                TextX = (config.charWidth * 2.9)
                TextY = (config.charHeight * 3)

                # Set position for SPECIAL attribute value
                TextValX = (config.charWidth * 22)
                TextValY = (config.charHeight * 3)

                # Draw page-names, with box around selected one
                for subPageNum in range(0, len(self.subPages)):

                    doSelBox = (subPageNum == thisPageNum)

                    BackColour = None
                    if doSelBox:
                        BoxColour = (config.SELBOXGREY, config.SELBOXGREY, config.SELBOXGREY)

                    # Assign names to list items for SPECIAL attributes
                    thisText = self.subPages[subPageNum].name

                    # Assign values to SPECIAL attributes
                    thisTextVal = config.specialDIC[self.subPages[subPageNum].name]['value']

                    # Assign Font and Colour etc
                    textImg = config.FONT_LRG.render(thisText, True, config.DRAWCOLOUR, (0, 0, 0))
                    textValImg = config.FONT_LRG.render(thisTextVal, True, config.DRAWCOLOUR, (0, 0, 0))

                    # Get width of string for SPECIAL attribute name
                    TextWidth = (textImg.get_width())
                    textPos = (TextX, TextY)

                    # Get width of string for SPECIAL attribute value
                    TextValWidth = (textValImg.get_width())
                    textValPos = (TextValX, TextValY)

                    if (doSelBox):
                        TextRect = (TextX - 2, TextY - 2, TextWidth + 4, config.charHeight + 4)
                        pygame.draw.rect(thisPage.pageListImg, BoxColour, TextRect, 0)
                        TextRect = (TextRect[0] - 2, TextRect[1], TextRect[2] + 2, TextRect[3])
                        pygame.draw.rect(thisPage.pageListImg, config.DRAWCOLOUR, TextRect, 2)

                    # print SPECIAL Names & values
                    thisPage.pageListImg.blit(textImg, textPos, None, pygame.BLEND_ADD)
                    thisPage.pageListImg.blit(textValImg, textValPos, None, pygame.BLEND_ADD)

                    # Move down so that next line is printed under current line
                    TextY += (config.charHeight * 1.5)
                    TextValY += (config.charHeight * 1.5)

                # Do initial page-draw, caching for later use:
                thisPage.drawPage()

        def drawPage(self):
            # Pass sub-page canvas up to tab-draw function...
            subPageCanvas, subPageChanged = self.curSubPage.drawPage()

            pageChanged = (self.changed or subPageChanged)
            self.changed = False

            return subPageCanvas, pageChanged

        # Called every time view changes to this page:
        def resetPage(self):
            self.subPageNum = 0
            self.curSubPage = self.subPages[self.subPageNum]

        # Consume events passed to this page:
        def ctrlEvents(self, events):

            for event in events:

                # Arrow-keys change subpage-number:
                if (type(event) is list):

                    listMove = event[2]
                    newPageNum = self.subPageNum - listMove

                    if (newPageNum < 0):
                        newPageNum = 0
                    elif (newPageNum >= len(self.subPages)):
                        newPageNum = (len(self.subPages) - 1)

                    if (newPageNum != self.subPageNum):
                        if config.USE_SOUND:
                            config.SOUNDS["changemode"].play()
                        self.subPageNum = newPageNum
                        self.curSubPage = self.subPages[newPageNum]
                        self.changed = True

            # Pass events to subpage too, just in case I've set them up as consumers too:
            #self.curSubPage.ctrlEvents(events)

    class Mode_Skills:

        # Used for line-based stats:
        class SkillLine:

            changed = True
            firstDraw = True

            frameNum = 0

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = args[1]
                self.pic = args[2]
                self.text = args[3]
                self.rankValue = args[4]
                self.starNum = self.rankValue
                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

            def drawPage(self):

                # Draw initial line-gauge:
                if (self.firstDraw):

                    self.firstDraw = False
                    img = self.pageListImg

                    # Assign current SKILL picture to variable
                    currentPic = self.pic
                    starPic = config.IMAGES["star"]

                    # Center the image of the SKILL above the text for it
                    img.blit(currentPic, ((config.WIDTH * 0.55), (config.HEIGHT * 0.1)))

                    cornerPadding = headFoot.cornerPadding
                    charWidth = config.charWidth
                    charHeight = config.charHeight

                    lineY = (config.HEIGHT * 0.55)
                    lineYDn = (lineY + (charHeight * 1.6))
                    lineDnAX = (config.WIDTH * 0.40)
                    lineRtX = (config.WIDTH - cornerPadding)

                    # Create variable spacers for star palcement
                    starX = (config.WIDTH * 0.65)
                    starY = (config.HEIGHT * 0.50)
                    for counter in range(0, self.starNum):
                        # First run only check if we need to align star centering
                        if counter == 0:
                            # Align star centering on horizontal depending on number of stars
                            if self.starNum == 5:
                                starX = (config.WIDTH * 0.57)
                            elif self.starNum == 4:
                                starX = (config.WIDTH * 0.59)
                            elif self.starNum == 3:
                                starX = (config.WIDTH * 0.61)
                            elif self.starNum == 2:
                                starX = (config.WIDTH * 0.63)
                            elif self.starNum == 1:
                                starX = (config.WIDTH * 0.65)
                            else:
                                sys.exit("1-5 Stars Max!!!")

                        img.blit(starPic, (starX, starY))
                        # Add to horizontal so next star is a spaced evenly to right
                        starX += 22
                        # starX *= 1.1

                    # Setup text box for word wrapping
                    textBoxWidth = (lineRtX - (lineDnAX + 6))
                    textBoxHight = (charHeight * 6)

                    # Draw text box wrapped text
                    my_font = config.FONT_MED
                    my_string = self.text
                    my_rect = pygame.Rect(lineDnAX + 6, lineY + 4, textBoxWidth, textBoxHight)
                    rendered_text = textrect.render_textrect(my_string, my_font, my_rect, config.DRAWCOLOUR, (0, 0, 0),
                                                         0)
                    if rendered_text:
                        img.blit(rendered_text, my_rect)

                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                    [(lineDnAX + 6, lineY), (lineRtX, lineY), (lineRtX, lineYDn)], 2)

                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    self.pageCanvas = self.pageListImg.convert()

                return self.pageCanvas, pageChanged

            # Called every view changes to this sub-page:
            def resetPage(self):
                True

        changed = True

        ######################################

        changed = True
        subPageNum = 0
        curSubPage = None

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "Skills"
            self.counter = 0
            self.textList = []
            self.textNumber = 1
            self.currentTXT = ''

            # Set up list of sub-pages:
            SkillLine = self.SkillLine
            ### TODO: Only show skills available for character level
            # Loop through parameters from file and create page entry in list for each.
            for key in sorted(config.skillsDIC):
                #print(config.skillsDIC[key]['name'])
                # print(config.skillsDIC[key]['filepath'])
                # print(config.skillsDIC[key]['text'])
                self.currentName = str(config.skillsDIC[key]['name'])
                self.currentPath = pygame.image.load(config.skillsDIC[key]['filepath'])
                # Find number of rank texts
                self.textList = str(config.skillsDIC[key]['text']).split('#')
                #pprint(self.textList)
                self.textNumber = int(len(self.textList))
                #print("Number of Ranks Texts= ", self.textNumber)
                #self.currentTXT = str(config.skillsDIC[key]['text'])
                self.currentRankValue = int(config.skillsDIC[key]['value'])
                #print("Current Rank Value= ", self.currentRankValue)
                # Now keep only the texts that are <= the rank
                self.currentTXT =  self.textList[self.currentRankValue - 1]
                ### TODO: We can't at moment list all perks. So list last one only.
                """
                for rankNumber in range(0, (self.currentRankValue)):
                    #print("Current Rank Text: ", str(self.textList[rankNumber]))
                    self.currentTXT += ' * ' + (str(self.textList[rankNumber]))
                    #print("CurrentTXT= ", self.currentTXT)
                """
                if self.counter <= 0:
                    self.subPages = [
                        SkillLine(self, self.currentName, self.currentPath, self.currentTXT, self.currentRankValue)]
                else:
                    self.subPages.extend(
                        [SkillLine(self, self.currentName, self.currentPath, self.currentTXT, self.currentRankValue)])
                self.counter += 1
                self.currentTXT = ''
                # print('Counter=', self.counter)
            """
            window_args = {"bg_color": (0, 0, 0),
                           "font": config.FONT_LRG,
                           "text_color": config.DRAWCOLOUR,
                           "text_offset": 5,
                           "sel_border_width": 2,
                           "sel_border_color": config.DRAWCOLOUR,
                           "sel_box_color": config.SELCOLOUR,
                           ### TODO store constants in config
                           "bar_rect": pygame.Rect((5, cornerPadding + cornerHeight + 4,
                                                    12, config.HEIGHT - 2 * (cornerHeight + cornerPadding) - 6)),
                           "bar_button_images": [setup.GFX["button_top"].subsurface((0, 0, 12, 6)),
                                                 setup.GFX["button_bottom"].subsurface((0, 0, 12, 6))],
                           }

            bar_subrect = Subrect(
                (config.charWidth * 2.9, config.charHeight * 3, config.WIDTH * 0.35, config.HEIGHT * 0.75))
            self.scroll_window = scrollwindow.ScrollWindow(bar_subrect,
                                                           [str(config.skillsDIC[key]['name']) for key in
                                                            sorted(config.skillsDIC)],
                                                           **window_args)
            """
            self.scroll_window = create_scrollmenu(
                [str(config.skillsDIC[key]['name']) for key in sorted(config.skillsDIC)])

            # Generate list-image for each sub-page:
            for thisPageNum in range(0, len(self.subPages)):
                thisPage = self.subPages[thisPageNum]
                thisPage.pageListImg = pygame.Surface((config.WIDTH, config.HEIGHT))

                # Do initial page-draw, caching for later use:
                thisPage.drawPage()

        def drawPage(self):
            # Pass sub-page canvas up to tab-draw function...
            subPageCanvas, subPageChanged = self.curSubPage.drawPage()
            scrollUpdate = self.scroll_window.update(subPageCanvas)

            pageChanged = (self.changed or subPageChanged or scrollUpdate)
            self.changed = False

            return subPageCanvas, pageChanged

        # Called every time view changes to this page:
        def resetPage(self):
            self.scroll_window.index = 0
            self.scroll_window.highlight_index = 0
            self.subPageNum = 0
            self.curSubPage = self.subPages[self.subPageNum]

        # Consume events passed to this page:
        def ctrlEvents(self, events):

            for event in events:
                self.scroll_window.get_events(event)
                newPageNum = self.scroll_window.highlight_index
                if (newPageNum != self.subPageNum):
                    if config.USE_SOUND:
                        config.SOUNDS["changemode"].play()
                    self.subPageNum = newPageNum
                    self.curSubPage = self.subPages[newPageNum]
                    self.changed = True

                    # Pass events to subpage too, just in case I've set them up as consumers too:
                    # self.curSubPage.ctrlEvents(events)

    class Mode_Perks:

        # Used for line-based stats:
        class PerkLine:

            changed = True
            firstDraw = True

            frameNum = 0

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = args[1]
                self.pic = args[2]
                self.text = args[3]
                self.starNum = args[4]
                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

            def drawPage(self):

                # Draw initial line-gauge:
                if (self.firstDraw):

                    self.firstDraw = False
                    img = self.pageListImg

                    # Assign current PERK picture to variable
                    currentPic = self.pic
                    starPic = config.IMAGES["star"]

                    # Center the image of the PERK above the text for it
                    img.blit(currentPic, ((config.WIDTH * 0.55), (config.HEIGHT * 0.1)))

                    cornerPadding = headFoot.cornerPadding
                    charWidth = config.charWidth
                    charHeight = config.charHeight

                    lineY = (config.HEIGHT * 0.55)
                    lineYDn = (lineY + (charHeight * 1.6))
                    lineDnAX = (config.WIDTH * 0.40)
                    lineRtX = (config.WIDTH - cornerPadding)

                    # Create variable spacers for star palcement
                    starX = (config.WIDTH * 0.65)
                    starY = (config.HEIGHT * 0.50)
                    for counter in range(0, self.starNum):
                        # First run only check if we need to align star centering
                        if counter == 0:
                            # Align star centering on horizontal depending on number of stars
                            if self.starNum == 4:
                                starX = (config.WIDTH * 0.59)
                            elif self.starNum == 3:
                                starX = (config.WIDTH * 0.61)
                            elif self.starNum == 2:
                                starX = (config.WIDTH * 0.63)
                            elif self.starNum == 1:
                                starX = (config.WIDTH * 0.65)
                            else:
                                sys.exit("1-4 Stars Max!!!")
                        img.blit(starPic, (starX, starY))
                        # Add to horizontal so next star is a spaced evenly to right
                        starX += 22
                        # starX *= 1.1

                    # Setup text box for word wrapping
                    textBoxWidth = (lineRtX - (lineDnAX + 6))
                    textBoxHeight = (charHeight * 5)

                    # Draw text box wrapped text
                    my_font = config.FONT_MED
                    my_string = self.text
                    my_rect = pygame.Rect(lineDnAX + 6, lineY + 4, textBoxWidth, textBoxHeight)
                    rendered_text = textrect.render_textrect(my_string, my_font, my_rect, config.DRAWCOLOUR, (0, 0, 0),
                                                             0)
                    if rendered_text:
                        img.blit(rendered_text, my_rect)

                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX + 6, lineY), (lineRtX, lineY), (lineRtX, lineYDn)], 2)

                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    self.pageCanvas = self.pageListImg.convert()

                return self.pageCanvas, pageChanged

            # Called every view changes to this sub-page:
            def resetPage(self):
                True

        changed = True

        ######################################

        changed = True
        subPageNum = 0
        curSubPage = None

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "Perks"
            self.counter = 0

            # Set up list of sub-pages:
            PerkLine = self.PerkLine

            ### Need to add scroller

            # Loop through parameters from file and create page entry in list for each.
            for key in sorted(config.perksDIC):
                # print(config.perksDIC[key]['name'])
                # print(config.perksDIC[key]['value'])
                # print(config.perksDIC[key]['filepath'])
                # print(config.perksDIC[key]['text'])
                self.currentName = str(config.perksDIC[key]['name'])
                self.currentValue = int(config.perksDIC[key]['value'])
                self.currentPath = pygame.image.load(config.perksDIC[key]['filepath'])
                self.currentTXT = str(config.perksDIC[key]['text'])
                if self.counter <= 0:
                    self.subPages = [PerkLine(self, self.currentName, self.currentPath, self.currentTXT, self.currentValue)]
                else:
                    self.subPages.extend([PerkLine(self, self.currentName, self.currentPath, self.currentTXT, self.currentValue)])
                self.counter += 1
                # print('Counter=', self.counter)
            """
            window_args = {"bg_color": (0, 0, 0),
                           "font": config.FONT_LRG,
                           "text_color": config.DRAWCOLOUR,
                           "text_offset": 5,
                           "sel_border_width": 2,
                           "sel_border_color": config.DRAWCOLOUR,
                           "sel_box_color": config.SELCOLOUR,
                           ### TODO store constants in config
                           "bar_rect": pygame.Rect((5, cornerPadding + cornerHeight + 4,
                                                    12, config.HEIGHT - 2 * (cornerHeight + cornerPadding) - 6)),
                           "bar_button_images": [setup.GFX["button_top"].subsurface((0, 0, 12, 6)),
                                                 setup.GFX["button_bottom"].subsurface((0, 0, 12, 6))],
                           }

            bar_subrect = Subrect(
                (config.charWidth * 2.9, config.charHeight * 3, config.WIDTH * 0.35, config.HEIGHT * 0.75))
            self.scroll_window = scrollwindow.ScrollWindow(bar_subrect,
                                                           [str(config.perksDIC[key]['name']) for key in
                                                            sorted(config.perksDIC)],
                                                           **window_args)
            """
            self.scroll_window = create_scrollmenu(
                [str(config.perksDIC[key]['name']) for key in sorted(config.perksDIC)])

            # Generate list-image for each sub-page:
            for thisPageNum in range(0, len(self.subPages)):
                thisPage = self.subPages[thisPageNum]
                thisPage.pageListImg = pygame.Surface((config.WIDTH, config.HEIGHT))

                # Do initial page-draw, caching for later use:
                thisPage.drawPage()

        def drawPage(self):
            # Pass sub-page canvas up to tab-draw function...
            subPageCanvas, subPageChanged = self.curSubPage.drawPage()
            scrollUpdate = self.scroll_window.update(subPageCanvas)

            pageChanged = (self.changed or subPageChanged or scrollUpdate)
            self.changed = False

            return subPageCanvas, pageChanged

        # Called every time view changes to this page:
        def resetPage(self):
            self.scroll_window.index = 0
            self.scroll_window.highlight_index = 0
            self.subPageNum = 0
            self.curSubPage = self.subPages[self.subPageNum]

        # Consume events passed to this page:
        def ctrlEvents(self, events):

            for event in events:
                self.scroll_window.get_events(event)
                newPageNum = self.scroll_window.highlight_index
                if (newPageNum != self.subPageNum):
                    if config.USE_SOUND:
                        config.SOUNDS["changemode"].play()
                    self.subPageNum = newPageNum
                    self.curSubPage = self.subPages[newPageNum]
                    self.changed = True

                    # Pass events to subpage too, just in case I've set them up as consumers too:
                    # self.curSubPage.ctrlEvents(events)

    class Mode_General:

        # Used for line-based stats:
        class GenLine:

            changed = True
            firstDraw = True

            frameNum = 0

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = args[1]
                self.pic = args[2]
                self.text = args[3]
                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

            def drawPage(self):

                # Draw initial line-gauge:
                if (self.firstDraw):

                    self.firstDraw = False
                    img = self.pageListImg

                    # Assign current GENERAL picture to variable
                    currentPic = self.pic

                    # Center the image of the General item above the text for it
                    img.blit(currentPic, ((config.WIDTH * 0.50), (config.HEIGHT * 0.0000000001)))

                    cornerPadding = headFoot.cornerPadding
                    charWidth = config.charWidth
                    charHeight = config.charHeight

                    lineY = (config.HEIGHT * 0.55)
                    lineYDn = (lineY + (charHeight * 1.6))
                    lineDnAX = (config.WIDTH * 0.40)
                    lineRtX = (config.WIDTH - cornerPadding)

                    # Setup text box for word wrapping
                    textBoxWidth = (lineRtX - (lineDnAX + 6))
                    textBoxHight = (charHeight * 5)

                    # Draw text box wrapped text
                    my_font = config.FONT_MED
                    my_string = self.text
                    my_rect = pygame.Rect(lineDnAX + 6, lineY + 4, textBoxWidth, textBoxHight)
                    rendered_text = textrect.render_textrect(my_string, my_font, my_rect, config.DRAWCOLOUR, (0, 0, 0),
                                                             0)
                    if rendered_text:
                        img.blit(rendered_text, my_rect)

                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX + 6, lineY), (lineRtX, lineY), (lineRtX, lineYDn)], 2)

                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    self.pageCanvas = self.pageListImg.convert()

                return self.pageCanvas, pageChanged

            # Called every view changes to this sub-page:
            def resetPage(self):
                True

        changed = True

        ######################################

        changed = True
        subPageNum = 0
        curSubPage = None

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "General"
            self.counter = 0

            # Set up list of sub-pages:
            GenLine = self.GenLine

            # Loop through parameters from file and create page entry in list for each.
            for key in sorted(config.generalDIC):
                # print(config.generalDIC[key]['name'])
                # print(config.generalDIC[key]['filepath'])
                # print(config.generalDIC[key]['text'])
                self.currentName = str(config.generalDIC[key]['name'])
                self.currentPath = pygame.image.load(config.generalDIC[key]['filepath'])
                self.currentTXT = str(config.generalDIC[key]['text'])
                if self.counter <= 0:
                    self.subPages = [
                        GenLine(self, self.currentName, self.currentPath, self.currentTXT)]
                else:
                    self.subPages.extend(
                        [GenLine(self, self.currentName, self.currentPath, self.currentTXT)])
                self.counter += 1
                # print('Counter=', self.counter)
            """
            window_args = {"bg_color": (0, 0, 0),
                           "font": config.FONT_LRG,
                           "text_color": config.DRAWCOLOUR,
                           "text_offset": 5,
                           "sel_border_width": 2,
                           "sel_border_color": config.DRAWCOLOUR,
                           "sel_box_color": config.SELCOLOUR,
                           ### TODO store constants in config
                           "bar_rect": pygame.Rect((5, cornerPadding + cornerHeight + 4,
                                                    12, config.HEIGHT - 2 * (cornerHeight + cornerPadding) - 6)),
                           "bar_button_images": [setup.GFX["button_top"].subsurface((0, 0, 12, 6)),
                                                 setup.GFX["button_bottom"].subsurface((0, 0, 12, 6))],
                           }

            bar_subrect = Subrect(
                (config.charWidth * 2.9, config.charHeight * 3, config.WIDTH * 0.35, config.HEIGHT * 0.75))

            self.scroll_window = scrollwindow.ScrollWindow(bar_subrect,
                                                           [str(config.generalDIC[key]['name']) for key in
                                                            sorted(config.generalDIC)],
                                                           **window_args)
            """
            self.scroll_window = create_scrollmenu(
                [str(config.generalDIC[key]['name']) for key in sorted(config.generalDIC)])

            # Generate list-image for each sub-page:
            for thisPageNum in range(0, len(self.subPages)):
                thisPage = self.subPages[thisPageNum]
                thisPage.pageListImg = pygame.Surface((config.WIDTH, config.HEIGHT))

                # Do initial page-draw, caching for later use:
                thisPage.drawPage()

        def drawPage(self):
            # Pass sub-page canvas up to tab-draw function...
            subPageCanvas, subPageChanged = self.curSubPage.drawPage()
            scrollUpdate = self.scroll_window.update(subPageCanvas)

            pageChanged = (self.changed or subPageChanged or scrollUpdate)
            self.changed = False

            return subPageCanvas, pageChanged

        # Called every view changes to this page:
        def resetPage(self):
            self.scroll_window.index = 0
            self.scroll_window.highlight_index = 0
            self.subPageNum = 0
            self.curSubPage = self.subPages[self.subPageNum]

        # Consume events passed to this page:
        def ctrlEvents(self, events):

            for event in events:
                self.scroll_window.get_events(event)
                newPageNum = self.scroll_window.highlight_index
                if (newPageNum != self.subPageNum):
                    if config.USE_SOUND:
                        config.SOUNDS["changemode"].play()
                    self.subPageNum = newPageNum
                    self.curSubPage = self.subPages[newPageNum]
                    self.changed = True

                    # Pass events to subpage too, just in case I've set them up as consumers too:
                    # self.curSubPage.ctrlEvents(events)

    # Generate text for header:
    def getHeaderText(self):
        return [self.name, "LVL %s" % (config.PLAYERLEVEL), "HP %s/%s" % (config.CURRHP, config.MAXHP),
                "AP %s/%s" % (config.CURRAP, config.MAXAP), "XP %s/%s" % (config.CURRXP, config.MAXXP), ]

    # Trigger page-functions
    def drawPage(self, modeNum):
        pageCanvas, pageChanged = self.modes[modeNum].drawPage()
        return pageCanvas, pageChanged

    def resetPage(self, modeNum):
        self.modes[modeNum].resetPage()

    def ctrlEvents(self, pageEvents, modeNum):
        self.modes[modeNum].ctrlEvents(pageEvents)

    # Tab init:
    def __init__(self, *args, **kwargs):
        self.parent = args[0]
        self.rootParent = self.parent.rootParent
        self.canvas = pygame.Surface((config.WIDTH, config.HEIGHT))
        self.drawnPageNum = -1

        self.modes = [self.Mode_Status(self), self.Mode_SPECIAL(self), self.Mode_Skills(self), self.Mode_Perks(self),
                      self.Mode_General(self)]

        self.modeNames = ["", "", "", "", ""]
        for n in range(0, 5):
            self.modeNames[n] = self.modes[n].name

        self.header = headFoot.Header(self)

        # Generate footers for mode-pages:
        self.footerImgs = headFoot.genFooterImgs(self.modeNames)
