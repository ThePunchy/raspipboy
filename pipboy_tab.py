# RasPipBoy: A Pip-Boy 3000 implementation for Raspberry Pi
#	Neal D Corbett, 2013
# Class for 'Data' tab

import pygame

import utils, config
import pipboy_headFoot as headFoot


class PipBoyTab:
    # Generate text for header:
    def getHeaderText(self):
        return [self.name, self.rootParent.gpsModule.locality, utils.getTimeStr(), ]

    # Trigger page-functions
    def drawPage(self, modeNum):
        pageCanvas, pageChanged = self.modes[modeNum].drawPage()
        return pageCanvas, pageChanged

    def resetPage(self, modeNum):
        self.modes[modeNum].resetPage()

    def ctrlEvents(self, pageEvents, modeNum):
        self.modes[modeNum].ctrlEvents(pageEvents)

    def __init__(self, parent, name, modes):
        self.parent = parent
        self.name = name
        self.rootParent = self.parent.rootParent
        self.canvas = pygame.Surface((config.WIDTH, config.HEIGHT))
        self.drawnPageNum = -1

        self.modes = modes(self)

        # Generate footers for mode-pages:
        self.modeNames = ["", "", "", "", ""]
        for n in range(0, len(self.modes)):
            self.modeNames[n] = self.modes[n].name

        self.header = headFoot.Header(self)

        # Generate footers for mode-pages:
        self.footerImgs = headFoot.genFooterImgs(self.modeNames)
