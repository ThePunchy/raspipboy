# RasPipBoy
*Original by **Neal D Corbett***
*Modifications by **JPB***


This is the [Pip-Boy 3000 user interface](http://www.youtube.com/watch?v=2iHNDYLm9-A&list=PLickeZlNSDh1gQIhxbzHKZVxq465AhWfz&index=1) from the game [*Fallout 3*](http://fallout.bethsoft.com/), written in Python for the [Raspberry Pi](http://www.raspberrypi.org/) single-board computer.

~~I may attempt to get this working on Android in future too.~~

## Features
* Reasonably accurate simulation of the *Fallout 3* Pip-Boy interface!
    * (at least the bits I've completed so far...)
* GPS-enabled map
* V.A.T.S. camera option
* Wifi-strength meter
* NOT RADIOACTIVE
* Added hardware controls using rotary switch and encoder
* Display on 3.5" TFT
* Easily modifiable values for all attributes (like weapons, S.P.E.C.I.A.L. etc.)
* Support for Python3
* "Radio" support for playing audio-files
* Lots of image content

## Original Documentation
Wiki documentation is [here](https://bitbucket.org/selectnone/raspipboy/wiki/Home)

## Source
Original Source is kept in my [Bitbucket](https://bitbucket.org/selectnone/raspipboy/src)
Modified Source is kept in [Bitbucket](https://bitbucket.org/jorgenboberg/raspipboy/src)