# RasPipBoy: A Pip-Boy 3000 implementation for Raspberry Pi
#	Neal D Corbett, 2013
# Class for 'Items' tab

import pygame, os, time, random, math, string, numpy
from pygame.locals import *
import config
import textrect
import pipboy_headFoot as headFoot
from config import cornerHeight, cornerPadding
from guipack import scrollwindow, setup
from guipack.subrect import Subrect
import sys
from pipboy_scrollmenu import create_scrollmenu

class Tab_Items:
    name = "ITEMS"

    class Mode_Weapons:

        # Used for line-based stats:
        class WeaponLine:

            changed = True
            firstDraw = True

            frameNum = 0

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = args[1]
                self.pic = args[2]
                self.dmg = args[3]
                self.wg = args[4]
                self.val = args[5]
                self.cnd = args[6]
                self.mf = '--'
                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))


            def drawPage(self):

                # Draw initial line-gauge:
                if (self.firstDraw):

                    self.firstDraw = False
                    img = self.pageListImg

                    # Assign current weapon picture to variable
                    currentPic = self.pic

                    # Center the image of the Weapon above the text for it
                    img.blit(currentPic, ((config.WIDTH * 0.60), (config.HEIGHT * 0.1)))

                    cornerPadding = headFoot.cornerPadding
                    charWidth = config.charWidth
                    charHeight = config.charHeight

                    # Height on page
                    lineY = (config.HEIGHT * 0.52)
                    # Line Y Down
                    lineYDn = (lineY + (charHeight * 1.6))
                    # Line from right to left
                    lineDnAX = (config.WIDTH * 0.40)
                    # Line to right hand side minus padding
                    lineRtX = (config.WIDTH - cornerPadding)

                    lineDnAX1 = (config.WIDTH * 0.80)
                    lineDnAX2 = (config.WIDTH * 0.60)
                    lineDnAX3 = (config.WIDTH * 0.40)

                    # Setup text box for word wrapping
                    textBoxWidth = (lineRtX - (lineDnAX + 6))
                    #textBoxWidth1 = ((lineRtX - 190) - (lineDnAX3 + 6))
                    textBoxWidth1 = 21 # Each char is 7 pixels in med font * 3 char
                    textBoxWidth2 = 14
                    textBoxWidth3 = 49
                    textBoxWidth4 = 28
                    textBoxWidth5 = 50
                    textBoxWidth6 = 84
                    textBoxHeight = (charHeight * 1)

                    # Draw text box wrapped text
                    my_font = config.FONT_MED

                    # Set label for the different status attributes
                    dam_str = "DMG"
                    wg_str = "WG"
                    val_str = "VAL"
                    cnd_str = "CND"
                    mf_str = "MF Cell"

                    # Get String length for CND
                    cndStrLen = len(cnd_str * config.charWidth)

                    # Setup values for different status attributes
                    dam_value = self.dmg
                    wg_value = self.wg
                    val_value = self.val
                    cnd_value = self.cnd
                    mf_value = self.mf

                    # Create the text boxes for each attribute string
                    dam_rect_str = pygame.Rect(lineDnAX3 + 6, lineY + 4, textBoxWidth1, textBoxHeight)
                    wg_rect_str = pygame.Rect(lineDnAX2, lineY + 4, textBoxWidth2, textBoxHeight)
                    val_rect_str = pygame.Rect(lineDnAX1, lineY + 4, textBoxWidth1, textBoxHeight)
                    cnd_rect_str = pygame.Rect(lineDnAX3 +6, lineY + 44, textBoxWidth1, textBoxHeight)
                    mf_rect_str = pygame.Rect(lineDnAX2, lineY + 44, textBoxWidth3, textBoxHeight)

                    # Find length of attribute value so we can right align
                    dmgValLength = 77 - ((len(dam_value) * 7) + 2)
                    wgValLength = 77 - ((len(wg_value) * 7) + 2)
                    valValLength = 77 -((len(val_value) * 7) + 2)
                    cndValLength = 77 - ((len(cnd_value) * 7) + 2)
                    mfValLength = 178 - ((len(mf_value) * 7) + 2)

                    # Create the text boxes for each attribute value
                    dam_rect_val = pygame.Rect(lineDnAX3 + dmgValLength, lineY + 4, textBoxWidth4, textBoxHeight)
                    wg_rect_val = pygame.Rect(lineDnAX2 + wgValLength, lineY + 4, textBoxWidth4, textBoxHeight)
                    val_rect_val = pygame.Rect(lineDnAX1 + valValLength, lineY + 4, textBoxWidth4, textBoxHeight)
                    cnd_rect_val = pygame.Rect(lineDnAX3 + cndValLength, lineY + 44, textBoxWidth5, textBoxHeight)
                    mf_rect_val = pygame.Rect(lineDnAX2 + mfValLength, lineY + 44, textBoxWidth6, textBoxHeight)

                    # Render the attribute Lables
                    rendered_text_dam_str = textrect.render_textrect(dam_str, my_font, dam_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0),0)
                    rendered_text_wg_str = textrect.render_textrect(wg_str, my_font, wg_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)
                    rendered_text_val_str = textrect.render_textrect(val_str, my_font, val_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)
                    rendered_text_cnd_str = textrect.render_textrect(cnd_str, my_font, cnd_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)
                    rendered_text_mf_str = textrect.render_textrect(mf_str, my_font, mf_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)

                    # Render the attribute values
                    rendered_text_dam_val = textrect.render_textrect(dam_value, my_font, dam_rect_val, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)
                    rendered_text_wg_val = textrect.render_textrect(wg_value, my_font, wg_rect_val, config.DRAWCOLOUR,
                                                                    (0, 0, 0), 0)
                    rendered_text_val_val = textrect.render_textrect(val_value, my_font, val_rect_val, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)
                    rendered_text_cnd_val = textrect.render_textrect(cnd_value, my_font, cnd_rect_val, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)
                    rendered_text_mf_val = textrect.render_textrect(mf_value, my_font, mf_rect_val, config.DRAWCOLOUR,
                                                                    (0, 0, 0), 0)

                    # Blit text to screen
                    if (rendered_text_dam_str and rendered_text_dam_val):
                        img.blit(rendered_text_dam_str, dam_rect_str)
                        img.blit(rendered_text_dam_val, dam_rect_val)
                    if (rendered_text_wg_str and rendered_text_wg_val):
                        img.blit(rendered_text_wg_str, wg_rect_str)
                        img.blit(rendered_text_wg_val, wg_rect_val)
                    if (rendered_text_val_str and rendered_text_val_val):
                        img.blit(rendered_text_val_str, val_rect_str)
                        img.blit(rendered_text_val_val, val_rect_val)
                    if (rendered_text_cnd_str and rendered_text_cnd_val):
                        img.blit(rendered_text_cnd_str, cnd_rect_str)
                        #img.blit(rendered_text_cnd_val, cnd_rect_val)
                    if (rendered_text_mf_str and rendered_text_mf_val):
                        img.blit(rendered_text_mf_str, mf_rect_str)
                        img.blit(rendered_text_mf_val, mf_rect_val)


                    # Draw the lines for each status item
                    # DAM
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX3 + 6, lineY), (lineRtX - 190, lineY), (lineRtX - 190, lineYDn)], 2)
                    # WG
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX2, lineY), (lineRtX - 95, lineY), (lineRtX - 95, lineYDn)], 2)
                    # VAL
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX1, lineY), (lineRtX, lineY), (lineRtX, lineYDn)], 2)
                    # CND
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX3 + 6, lineY + 40), (lineRtX - 190, lineY + 40),
                                       (lineRtX - 190, lineYDn + 40)], 2)
                    # MF CELL
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX2, lineY + 40), (lineRtX, lineY + 40), (lineRtX, lineYDn + 40)], 2)

                    # Draw CND status bar (field rectangle within outline)

                    pygame.draw.rect(img, config.SELCOLOUR, pygame.Rect((cndStrLen + 5) + (lineDnAX3 + 6), lineY + 49, textBoxWidth5,
                                                                    (textBoxHeight / 2)))

                    pygame.draw.rect(img, config.DRAWCOLOUR, pygame.Rect((cndStrLen + 5) + (lineDnAX3 + 6), lineY + 49, (textBoxWidth5 * (int(cnd_value) / 100)) ,
                                                                       (textBoxHeight / 2)))

                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    self.pageCanvas = self.pageListImg.convert()

                return self.pageCanvas, pageChanged

            # Called every view changes to this sub-page:
            def resetPage(self):
                True

        changed = True

        ######################################

        changed = True
        subPageNum = 0
        curSubPage = None

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "Weapons"
            self.counter = 0

            # Set up list of sub-pages:
            weapons = self.WeaponLine

            # Loop through parameters from file and create page entry in list for each.
            for key in sorted(config.weaponsDIC):
                #print(config.weaponsDIC[key]['name'])
                #print(config.weaponsDIC[key]['filepath'])
                #print(config.weaponsDIC[key]['DMG'])
                #print(config.weaponsDIC[key]['WG'])
                #print(config.weaponsDIC[key]['VAL'])
                #print(config.weaponsDIC[key]['CND'])
                #print(config.weaponsDIC[key]['MF Cell'])
                self.currentName = str(config.weaponsDIC[key]['name'])
                self.currentPath = pygame.image.load(config.weaponsDIC[key]['filepath'])
                self.currentDMG = str(config.weaponsDIC[key]['DMG'])
                self.currentWG = str(config.weaponsDIC[key]['WG'])
                self.currentVAL = str(config.weaponsDIC[key]['VAL'])
                self.currentCND = str(config.weaponsDIC[key]['CND'])
                #self.currentMF = str(config.weaponsDIC[key][MF Cell])
                if self.counter <= 0:
                    #self.subPages = [weapons(self, self.currentName, self.currentPath, self.currentDMG, self.currentWG, self.currentVAL, self.currentCND, self.currentMF)]
                    self.subPages = [weapons(self, self.currentName, self.currentPath, self.currentDMG, self.currentWG,
                                             self.currentVAL, self.currentCND)]
                else:
                    #self.subPages.extend([weapons(self, self.currentName, self.currentPath, self.currentDMG, self.currentWG, self.currentVAL, self.currentCND, self.currentMF)])
                    self.subPages.extend([weapons(self, self.currentName, self.currentPath, self.currentDMG,
                                                  self.currentWG, self.currentVAL, self.currentCND)])
                self.counter += 1
                #print('Counter=', self.counter)

            amounts = list(map(lambda x: '' if x < 1 else '(' + str(x) + ')',
                          (int(config.weaponsDIC[key]['amount']) for key in sorted(config.weaponsDIC))))
            """
            window_args = {"bg_color": (0, 0, 0),
                           "font": config.FONT_LRG,
                           "text_color": config.DRAWCOLOUR,
                           "text_offset": 5,
                           "sel_border_width": 2,
                           "sel_border_color": config.DRAWCOLOUR,
                           "sel_box_color": config.SELCOLOUR,
                           ### TODO store constants in config
                           "bar_rect": pygame.Rect((5, cornerPadding + cornerHeight + 4,
                                                    12, config.HEIGHT - 2 * (cornerHeight + cornerPadding) - 6)),
                           "bar_button_images": [setup.GFX["button_top"].subsurface((0, 0, 12, 6)),
                                                 setup.GFX["button_bottom"].subsurface((0, 0, 12, 6))],
                           "amounts": amounts
                           }

            bar_subrect = Subrect(
                    (config.charWidth * 2.9, config.charHeight * 3, config.WIDTH * 0.35, config.HEIGHT * 0.75))
            """
            self.scroll_window = create_scrollmenu([str(config.weaponsDIC[key]['name']) for key in sorted(config.weaponsDIC)],
                                                   amounts)

            # Generate list-image for each sub-page:
            for thisPageNum in range(0, len(self.subPages)):
            #for thisPageNum in range(0, self.counter):

                thisPage = self.subPages[thisPageNum]
                thisPage.pageListImg = pygame.Surface((config.WIDTH, config.HEIGHT))

                # Do initial page-draw, caching for later use:
                thisPage.drawPage()

        def drawPage(self):
            # Pass sub-page canvas up to tab-draw function...
            subPageCanvas, subPageChanged = self.curSubPage.drawPage()
            scrollUpdate = self.scroll_window.update(subPageCanvas)

            pageChanged = (self.changed or subPageChanged or scrollUpdate)
            self.changed = False

            return subPageCanvas, pageChanged

        # Called every time view changes to this page:
        def resetPage(self):
            self.scroll_window.index = 0
            self.scroll_window.highlight_index = 0
            self.subPageNum = 0
            self.curSubPage = self.subPages[self.subPageNum]

        # Consume events passed to this page:
        def ctrlEvents(self, events):

            for event in events:
                self.scroll_window.get_events(event)
                newPageNum = self.scroll_window.highlight_index
                if (newPageNum != self.subPageNum):
                    if config.USE_SOUND:
                        config.SOUNDS["changemode"].play()
                    self.subPageNum = newPageNum
                    self.curSubPage = self.subPages[newPageNum]
                    self.changed = True

                # Pass events to subpage too, just in case I've set them up as consumers too:
                # self.curSubPage.ctrlEvents(events)

    class Mode_Apparel:

        # Used for line-based stats:
        class ApparelLine:

            changed = True
            firstDraw = True

            frameNum = 0

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = args[1]
                self.pic = args[2]
                self.dr = args[3]
                self.wg = args[4]
                self.val = args[5]
                self.cnd = args[6]
                self.efct = args[7]
                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

            def drawPage(self):

                # Draw initial line-gauge:
                if (self.firstDraw):

                    self.firstDraw = False
                    img = self.pageListImg

                    # Assign current apparel picture to variable
                    currentPic = self.pic

                    # Center the image of the Apparel above the text for it
                    img.blit(currentPic, ((config.WIDTH * 0.58), (config.HEIGHT * 0.1)))

                    cornerPadding = headFoot.cornerPadding
                    charWidth = config.charWidth
                    charHeight = config.charHeight

                    # Height on page
                    lineY = (config.HEIGHT * 0.52)
                    # Line Y Down
                    lineYDn = (lineY + (charHeight * 1.6))
                    # Line from right to left
                    lineDnAX = (config.WIDTH * 0.40)
                    # Line to right hand side minus padding
                    lineRtX = (config.WIDTH - cornerPadding)

                    lineDnAX1 = (config.WIDTH * 0.80)
                    lineDnAX2 = (config.WIDTH * 0.60)
                    lineDnAX3 = (config.WIDTH * 0.40)

                    # Setup text box for word wrapping
                    textBoxWidth = (lineRtX - (lineDnAX + 6))
                    #textBoxWidth1 = ((lineRtX - 190) - (lineDnAX3 + 6))
                    textBoxWidth1 = 21 # Each char is 7 pixels in med font * 3 char
                    textBoxWidth2 = 14
                    textBoxWidth3 = 49
                    textBoxWidth4 = 28
                    textBoxWidth5 = 50
                    textBoxWidth6 = 84
                    textBoxHeight = (charHeight * 1)

                    # Draw text box wrapped text
                    my_font = config.FONT_MED

                    # Set label for the different status attributes
                    dr_str = "DR"
                    wg_str = "WG"
                    val_str = "VAL"
                    cnd_str = "CND"
                    efct_str = "EFFECTS"

                    # Get String length for CND
                    cndStrLen = len(cnd_str * config.charWidth)

                    # Setup values for different status attributes
                    dr_value = self.dr
                    wg_value = self.wg
                    val_value = self.val
                    cnd_value = self.cnd
                    #print("EffectValue = " + str(self.efct))
                    if isinstance(self.efct, list):
                        efct_value = ', '.join(self.efct)
                    else:
                        efct_value = self.efct
                    #print("EffectValue = " + efct_value)

                    # Create the text boxes for each attribute string
                    dr_rect_str = pygame.Rect(lineDnAX3 + 6, lineY + 4, textBoxWidth1, textBoxHeight)
                    wg_rect_str = pygame.Rect(lineDnAX2, lineY + 4, textBoxWidth2, textBoxHeight)
                    val_rect_str = pygame.Rect(lineDnAX1, lineY + 4, textBoxWidth1, textBoxHeight)
                    cnd_rect_str = pygame.Rect(lineDnAX3 +6, lineY + 44, textBoxWidth1, textBoxHeight)
                    efct_rect_str = pygame.Rect(lineDnAX3 + 6, lineY + 84, textBoxWidth3, textBoxHeight)

                    # Find length of attribute value so we can right align
                    drValLength = 77 - ((len(dr_value) * 7) + 2)
                    wgValLength = 77 - ((len(wg_value) * 7) + 2)
                    valValLength = 77 -((len(val_value) * 7) + 2)
                    cndValLength = 77 - ((len(cnd_value) * 7) + 2)
                    efctValLength = 178 - ((len(efct_value) * 7) + 2)
                    efctStrLength = 178 - ((len(efct_str) * 7) + 2)

                    # Create the text boxes for each attribute value
                    dr_rect_val = pygame.Rect(lineDnAX3 + drValLength, lineY + 4, textBoxWidth4, textBoxHeight)
                    wg_rect_val = pygame.Rect(lineDnAX2 + wgValLength, lineY + 4, textBoxWidth4, textBoxHeight)
                    val_rect_val = pygame.Rect(lineDnAX1 + valValLength, lineY + 4, textBoxWidth4, textBoxHeight)
                    cnd_rect_val = pygame.Rect(lineDnAX3 + cndValLength, lineY + 44, textBoxWidth5, textBoxHeight)
                    efct_rect_val = pygame.Rect(lineDnAX2, lineY + 84, 180 , textBoxHeight * 3)

                    # Render the attribute Lables
                    rendered_text_dr_str = textrect.render_textrect(dr_str, my_font, dr_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0),0)
                    rendered_text_wg_str = textrect.render_textrect(wg_str, my_font, wg_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)
                    rendered_text_val_str = textrect.render_textrect(val_str, my_font, val_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)
                    rendered_text_cnd_str = textrect.render_textrect(cnd_str, my_font, cnd_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)
                    rendered_text_efct_str = textrect.render_textrect(efct_str, my_font, efct_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)

                    # Render the attribute values
                    rendered_text_dr_val = textrect.render_textrect(dr_value, my_font, dr_rect_val, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)
                    rendered_text_wg_val = textrect.render_textrect(wg_value, my_font, wg_rect_val, config.DRAWCOLOUR,
                                                                    (0, 0, 0), 0)
                    rendered_text_val_val = textrect.render_textrect(val_value, my_font, val_rect_val, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)
                    rendered_text_cnd_val = textrect.render_textrect(cnd_value, my_font, cnd_rect_val, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)
                    rendered_text_efct_val = textrect.render_textrect(efct_value, my_font, efct_rect_val, config.DRAWCOLOUR,
                                                                    (0, 0, 0), 0)

                    # Blit text to screen
                    if (rendered_text_dr_str and rendered_text_dr_val):
                        img.blit(rendered_text_dr_str, dr_rect_str)
                        img.blit(rendered_text_dr_val, dr_rect_val)
                    if (rendered_text_wg_str and rendered_text_wg_val):
                        img.blit(rendered_text_wg_str, wg_rect_str)
                        img.blit(rendered_text_wg_val, wg_rect_val)
                    if (rendered_text_val_str and rendered_text_val_val):
                        img.blit(rendered_text_val_str, val_rect_str)
                        img.blit(rendered_text_val_val, val_rect_val)
                    if (rendered_text_cnd_str and rendered_text_cnd_val):
                        img.blit(rendered_text_cnd_str, cnd_rect_str)
                        #img.blit(rendered_text_cnd_val, cnd_rect_val)
                    if (rendered_text_efct_str and rendered_text_efct_val):
                        img.blit(rendered_text_efct_str, efct_rect_str)
                        img.blit(rendered_text_efct_val, efct_rect_val)

                    # Draw the lines for each status item
                    # DR
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX3 + 6, lineY), (lineRtX - 190, lineY), (lineRtX - 190, lineYDn)], 2)
                    # WG
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX2, lineY), (lineRtX - 95, lineY), (lineRtX - 95, lineYDn)], 2)
                    # VAL
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX1, lineY), (lineRtX, lineY), (lineRtX, lineYDn)], 2)
                    # CND
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX3 + 6, lineY + 40), (lineRtX - 190, lineY + 40),
                                       (lineRtX - 190, lineYDn + 40)], 2)
                    # EFCT CELL
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX3 + 6, lineY + 80), (lineRtX, lineY + 80), (lineRtX, lineYDn + 80)], 2)

                    # Draw CND status bar (field rectangle within outline)

                    pygame.draw.rect(img, config.SELCOLOUR, pygame.Rect((cndStrLen + 5) + (lineDnAX3 + 6), lineY + 49, textBoxWidth5,
                                                                        (textBoxHeight / 2)))

                    pygame.draw.rect(img, config.DRAWCOLOUR, pygame.Rect((cndStrLen + 5) + (lineDnAX3 + 6), lineY + 49,(textBoxWidth5 * (int(cnd_value) / 100)),
                                                                         (textBoxHeight / 2)))

                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    self.pageCanvas = self.pageListImg.convert()

                return self.pageCanvas, pageChanged

            # Called every view changes to this sub-page:
            def resetPage(self):
                True

        changed = True

        ######################################

        changed = True
        subPageNum = 0
        curSubPage = None

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "Apparel"
            self.counter = 0

            # Set up list of sub-pages:
            ApparelLine = self.ApparelLine

            # Loop through parameters from file and create page entry in list for each.
            for key in sorted(config.apparelDIC):
                # print(config.apparelDIC[key]['name'])
                # print(config.apparelDIC[key]['filepath'])
                # print(config.apparelDIC[key]['DR'])
                # print(config.apparelDIC[key]['WG'])
                # print(config.apparelDIC[key]['VAL'])
                # print(config.apparelDIC[key]['CND'])
                # print(config.apparelDIC[key]['EFFECTS'])
                self.currentName = str(config.apparelDIC[key]['name'])
                self.currentPath = pygame.image.load(config.apparelDIC[key]['filepath'])
                self.currentDR = str(config.apparelDIC[key]['DR'])
                self.currentWG = str(config.apparelDIC[key]['WG'])
                self.currentVAL = str(config.apparelDIC[key]['VAL'])
                self.currentCND = str(config.apparelDIC[key]['CND'])
                self.currentEFCT = config.apparelDIC[key]['EFFECTS']
                if self.counter <= 0:
                    self.subPages = [ApparelLine(self, self.currentName, self.currentPath, self.currentDR, self.currentWG,
                                             self.currentVAL, self.currentCND, self.currentEFCT)]
                else:
                    self.subPages.extend([ApparelLine(self, self.currentName, self.currentPath, self.currentDR, self.currentWG,
                                             self.currentVAL, self.currentCND, self.currentEFCT)])
                self.counter += 1
                # print('Counter=', self.counter)
            """
            window_args = {"bg_color": (0, 0, 0),
                           "font": config.FONT_LRG,
                           "text_color": config.DRAWCOLOUR,
                           "text_offset": 5,
                           "sel_border_width": 2,
                           "sel_border_color": config.DRAWCOLOUR,
                           "sel_box_color": config.SELCOLOUR,
                           ### TODO store constants in config
                           "bar_rect": pygame.Rect((5, cornerPadding + cornerHeight + 4,
                                                    12, config.HEIGHT - 2 * (cornerHeight + cornerPadding) - 6)),
                           "bar_button_images": [setup.GFX["button_top"].subsurface((0, 0, 12, 6)),
                                                 setup.GFX["button_bottom"].subsurface((0, 0, 12, 6))],
                           }


            bar_subrect = Subrect(
                (config.charWidth * 2.9, config.charHeight * 3, config.WIDTH * 0.35, config.HEIGHT * 0.75))
            """
            self.scroll_window = create_scrollmenu(
                [str(config.apparelDIC[key]['name']) for key in sorted(config.apparelDIC)])

            # Generate list-image for each sub-page:
            for thisPageNum in range(0, len(self.subPages)):

                thisPage = self.subPages[thisPageNum]
                thisPage.pageListImg = pygame.Surface((config.WIDTH, config.HEIGHT))

                # Do initial page-draw, caching for later use:
                thisPage.drawPage()

        def drawPage(self):
            # Pass sub-page canvas up to tab-draw function...
            subPageCanvas, subPageChanged = self.curSubPage.drawPage()
            scrollUpdate = self.scroll_window.update(subPageCanvas)

            pageChanged = (self.changed or subPageChanged or scrollUpdate)
            self.changed = False

            return subPageCanvas, pageChanged

        # Called every time view changes to this page:
        def resetPage(self):
            self.scroll_window.index = 0
            self.scroll_window.highlight_index = 0
            self.subPageNum = 0
            self.curSubPage = self.subPages[self.subPageNum]

        # Consume events passed to this page:
        def ctrlEvents(self, events):

            for event in events:
                self.scroll_window.get_events(event)
                newPageNum = self.scroll_window.highlight_index
                if (newPageNum != self.subPageNum):
                    if config.USE_SOUND:
                        config.SOUNDS["changemode"].play()
                    self.subPageNum = newPageNum
                    self.curSubPage = self.subPages[newPageNum]
                    self.changed = True

                    # Pass events to subpage too, just in case I've set them up as consumers too:
                    # self.curSubPage.ctrlEvents(events)

    class Mode_Aid:

        # Used for line-based stats:
        class AidLine:

            changed = True
            firstDraw = True

            frameNum = 0

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = args[1]
                self.amount = args[2]
                self.pic = args[3]
                self.wg = args[4]
                self.val = args[5]
                self.efct = args[6]
                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

            def drawPage(self):

                # Draw initial line-gauge:
                if (self.firstDraw):

                    self.firstDraw = False
                    img = self.pageListImg

                    # Assign current aid picture to variable
                    currentPic = self.pic

                    # Center the image of the Aid above the text for it
                    img.blit(currentPic, ((config.WIDTH * 0.63), (config.HEIGHT * 0.1)))

                    cornerPadding = headFoot.cornerPadding
                    charWidth = config.charWidth
                    charHeight = config.charHeight

                    # Height on page
                    lineY = (config.HEIGHT * 0.52)
                    # Line Y Down
                    lineYDn = (lineY + (charHeight * 1.6))
                    # Line from right to left
                    lineDnAX = (config.WIDTH * 0.40)
                    # Line to right hand side minus padding
                    lineRtX = (config.WIDTH - cornerPadding)

                    lineDnAX1 = (config.WIDTH * 0.80)
                    lineDnAX2 = (config.WIDTH * 0.60)
                    lineDnAX3 = (config.WIDTH * 0.40)

                    # Setup text box for word wrapping
                    textBoxWidth = (lineRtX - (lineDnAX + 6))
                    #textBoxWidth1 = ((lineRtX - 190) - (lineDnAX3 + 6))
                    textBoxWidth1 = 21 # Each char is 7 pixels in med font * 3 char
                    textBoxWidth2 = 14
                    textBoxWidth3 = 49
                    textBoxWidth4 = 28
                    textBoxWidth5 = 50
                    textBoxWidth6 = 84
                    textBoxHeight = (charHeight * 1)

                    # Draw text box wrapped text
                    my_font = config.FONT_MED

                    # Set label for the different status attributes
                    wg_str = "WG"
                    val_str = "VAL"
                    efct_str = "EFFECTS"

                    # Setup values for different status attributes
                    wg_value = self.wg
                    val_value = self.val
                    if isinstance(self.efct, list):
                        efct_value = ', '.join(self.efct)
                    else:
                        efct_value = self.efct
                    #print("EffectValue = " + efct_value)


                    # Create the text boxes for each attribute string
                    wg_rect_str = pygame.Rect(lineDnAX2, lineY + 4, textBoxWidth2, textBoxHeight)
                    val_rect_str = pygame.Rect(lineDnAX1, lineY + 4, textBoxWidth1, textBoxHeight)
                    efct_rect_str = pygame.Rect(lineDnAX3 + 6, lineY + 44, textBoxWidth3, textBoxHeight)

                    # Find length of attribute value so we can right align
                    wgValLength = 77 - ((len(wg_value) * 7) + 2)
                    valValLength = 77 -((len(val_value) * 7) + 2)
                    efctValLength = 178 - ((len(efct_value) * 7) + 2)

                    # Create the text boxes for each attribute value
                    wg_rect_val = pygame.Rect(lineDnAX2 + wgValLength, lineY + 4, textBoxWidth4, textBoxHeight)
                    val_rect_val = pygame.Rect(lineDnAX1 + valValLength, lineY + 4, textBoxWidth4, textBoxHeight)
                    efct_rect_val = pygame.Rect(lineDnAX2, lineY + 44, 180, textBoxHeight * 5)

                    # Render the attribute Lables
                    rendered_text_wg_str = textrect.render_textrect(wg_str, my_font, wg_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)
                    rendered_text_val_str = textrect.render_textrect(val_str, my_font, val_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)
                    rendered_text_efct_str = textrect.render_textrect(efct_str, my_font, efct_rect_str, config.DRAWCOLOUR,
                                                                 (0, 0, 0), 0)

                    # Render the attribute values
                    rendered_text_wg_val = textrect.render_textrect(wg_value, my_font, wg_rect_val, config.DRAWCOLOUR,
                                                                    (0, 0, 0), 0)
                    rendered_text_val_val = textrect.render_textrect(val_value, my_font, val_rect_val, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)
                    rendered_text_efct_val = textrect.render_textrect(efct_value, my_font, efct_rect_val, config.DRAWCOLOUR,
                                                                    (0, 0, 0), 0)

                    # Blit text to screen
                    if (rendered_text_wg_str and rendered_text_wg_val):
                        img.blit(rendered_text_wg_str, wg_rect_str)
                        img.blit(rendered_text_wg_val, wg_rect_val)
                    if (rendered_text_val_str and rendered_text_val_val):
                        img.blit(rendered_text_val_str, val_rect_str)
                        img.blit(rendered_text_val_val, val_rect_val)
                    if (rendered_text_efct_str and rendered_text_efct_val):
                        img.blit(rendered_text_efct_str, efct_rect_str)
                        img.blit(rendered_text_efct_val, efct_rect_val)

                    # Draw the lines for each status item
                    # WG
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX2, lineY), (lineRtX - 95, lineY), (lineRtX - 95, lineYDn)], 2)
                    # VAL
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX1, lineY), (lineRtX, lineY), (lineRtX, lineYDn)], 2)
                    # EFCT CELL
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX3 + 6, lineY + 40), (lineRtX, lineY + 40), (lineRtX, lineYDn + 40)], 2)

                    ### TODO - Add status bar with percentage depenging on status of apparel (2 color one darker with percent.

                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    self.pageCanvas = self.pageListImg.convert()

                return self.pageCanvas, pageChanged


            # Called every view changes to this sub-page:
            def resetPage(self):
                True

        changed = True

        ######################################

        changed = True
        subPageNum = 0
        curSubPage = None

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "Aid"
            self.counter = 0

            # Set up list of sub-pages:
            AidLine = self.AidLine
            PoopLine1 = self.AidLine
            PoopLine2 = self.AidLine
            PoopLine3 = self.AidLine
            PoopLine4 = self.AidLine

            # Loop through parameters from file and create page entry in list for each.
            for key in sorted(config.aidDIC):
                # print(config.aidDIC[key]['name'])
                # print(config.aidDIC[key]['amount'])
                # print(config.aidDIC[key]['filepath'])
                # print(config.aidDIC[key]['WG'])
                # print(config.aidDIC[key]['VAL'])
                # print(config.aidDIC[key]['EFFECTS'])
                self.currentName = str(config.aidDIC[key]['name'])
                self.currentAmt = str(config.aidDIC[key]['amount'])
                self.currentPath = pygame.image.load(config.aidDIC[key]['filepath'])
                self.currentWG = str(config.aidDIC[key]['WG'])
                self.currentVAL = str(config.aidDIC[key]['VAL'])
                self.currentEFCT = config.aidDIC[key]['EFFECTS']
                if self.counter <= 0:
                    self.subPages = [AidLine(self, self.currentName, self.currentAmt, self.currentPath, self.currentWG,
                                             self.currentVAL, self.currentEFCT)]
                else:
                    self.subPages.extend([AidLine(self, self.currentName, self.currentAmt, self.currentPath, self.currentWG,
                                             self.currentVAL, self.currentEFCT)])
                self.counter += 1
                # print('Counter=', self.counter)

            amounts = list(map(lambda x: '' if x < 1 else '(' + str(x) + ')',
                               (int(config.aidDIC[key]['amount']) for key in sorted(config.aidDIC))))
            """
            window_args = {"bg_color": (0, 0, 0),
                           "font": config.FONT_LRG,
                           "text_color": config.DRAWCOLOUR,
                           "text_offset": 5,
                           "sel_border_width": 2,
                           "sel_border_color": config.DRAWCOLOUR,
                           "sel_box_color": config.SELCOLOUR,
                           ### TODO store constants in config
                           "bar_rect": pygame.Rect((5, cornerPadding + cornerHeight + 4,
                                                    12, config.HEIGHT - 2 * (cornerHeight + cornerPadding) - 6)),
                           "bar_button_images": [setup.GFX["button_top"].subsurface((0, 0, 12, 6)),
                                                 setup.GFX["button_bottom"].subsurface((0, 0, 12, 6))],
                           "amounts": amounts
                           }

            bar_subrect = Subrect(
                (config.charWidth * 2.9, config.charHeight * 3, config.WIDTH * 0.35, config.HEIGHT * 0.75))
            """
            self.scroll_window = create_scrollmenu([str(config.aidDIC[key]['name']) for key in sorted(config.aidDIC)],
                                                   amounts)

            # Generate list-image for each sub-page:
            for thisPageNum in range(0, len(self.subPages)):

                thisPage = self.subPages[thisPageNum]
                thisPage.pageListImg = pygame.Surface((config.WIDTH, config.HEIGHT))

                # Do initial page-draw, caching for later use:
                thisPage.drawPage()

        def drawPage(self):
            # Pass sub-page canvas up to tab-draw function...
            subPageCanvas, subPageChanged = self.curSubPage.drawPage()
            scrollUpdate = self.scroll_window.update(subPageCanvas)

            pageChanged = (self.changed or subPageChanged or scrollUpdate)
            self.changed = False

            return subPageCanvas, pageChanged

        # Called every time view changes to this page:
        def resetPage(self):
            self.scroll_window.index = 0
            self.scroll_window.highlight_index = 0
            self.subPageNum = 0
            self.curSubPage = self.subPages[self.subPageNum]

        # Consume events passed to this page:
        def ctrlEvents(self, events):

            for event in events:
                self.scroll_window.get_events(event)
                newPageNum = self.scroll_window.highlight_index
                if (newPageNum != self.subPageNum):
                    if config.USE_SOUND:
                        config.SOUNDS["changemode"].play()
                    self.subPageNum = newPageNum
                    self.curSubPage = self.subPages[newPageNum]
                    self.changed = True

                    # Pass events to subpage too, just in case I've set them up as consumers too:
                    # self.curSubPage.ctrlEvents(events)

    class Mode_Misc:

        # Used for line-based stats:
        class MiscLine:

            changed = True
            firstDraw = True

            frameNum = 0

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = args[1]
                self.amount = args[2]
                self.pic = args[3]
                self.wg = args[4]
                self.val = args[5]

                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

            def drawPage(self):

                # Draw initial line-gauge:
                if (self.firstDraw):

                    self.firstDraw = False
                    img = self.pageListImg

                    # Assign current aid picture to variable
                    currentPic = self.pic

                    # Center the image of the Aid above the text for it
                    img.blit(currentPic, ((config.WIDTH * 0.65), (config.HEIGHT * 0.1)))

                    cornerPadding = headFoot.cornerPadding
                    charWidth = config.charWidth
                    charHeight = config.charHeight

                    # Height on page
                    lineY = (config.HEIGHT * 0.52)
                    # Line Y Down
                    lineYDn = (lineY + (charHeight * 1.6))
                    # Line from right to left
                    lineDnAX = (config.WIDTH * 0.40)
                    # Line to right hand side minus padding
                    lineRtX = (config.WIDTH - cornerPadding)

                    lineDnAX1 = (config.WIDTH * 0.80)
                    lineDnAX2 = (config.WIDTH * 0.60)
                    lineDnAX3 = (config.WIDTH * 0.40)

                    # Setup text box for word wrapping
                    textBoxWidth = (lineRtX - (lineDnAX + 6))
                    textBoxWidth1 = 21  # Each char is 7 pixels in med font * 3 char
                    textBoxWidth2 = 14
                    textBoxWidth3 = 49
                    textBoxWidth4 = 28
                    textBoxWidth5 = 50
                    textBoxWidth6 = 84
                    textBoxHeight = (charHeight * 1)

                    # Draw text box wrapped text
                    my_font = config.FONT_MED

                    # Set label for the different status attributes
                    wg_str = "WG"
                    val_str = "VAL"

                    # Setup values for different status attributes
                    wg_value = self.wg
                    val_value = self.val

                    # Create the text boxes for each attribute string
                    wg_rect_str = pygame.Rect(lineDnAX2, lineY + 4, textBoxWidth2, textBoxHeight)
                    val_rect_str = pygame.Rect(lineDnAX1, lineY + 4, textBoxWidth1, textBoxHeight)

                    # Find length of attribute value so we can right align
                    wgValLength = 77 - ((len(wg_value) * 7) + 2)
                    valValLength = 77 - ((len(val_value) * 7) + 2)

                    # Create the text boxes for each attribute value
                    wg_rect_val = pygame.Rect(lineDnAX2 + wgValLength, lineY + 4, textBoxWidth4, textBoxHeight)
                    val_rect_val = pygame.Rect(lineDnAX1 + valValLength, lineY + 4, textBoxWidth4, textBoxHeight)

                    # Render the attribute Lables
                    rendered_text_wg_str = textrect.render_textrect(wg_str, my_font, wg_rect_str, config.DRAWCOLOUR,
                                                                    (0, 0, 0), 0)
                    rendered_text_val_str = textrect.render_textrect(val_str, my_font, val_rect_str, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)

                    # Render the attribute values
                    rendered_text_wg_val = textrect.render_textrect(wg_value, my_font, wg_rect_val, config.DRAWCOLOUR,
                                                                    (0, 0, 0), 0)
                    rendered_text_val_val = textrect.render_textrect(val_value, my_font, val_rect_val, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)

                    # Blit text to screen
                    if (rendered_text_wg_str and rendered_text_wg_val):
                        img.blit(rendered_text_wg_str, wg_rect_str)
                        img.blit(rendered_text_wg_val, wg_rect_val)
                    if (rendered_text_val_str and rendered_text_val_val):
                        img.blit(rendered_text_val_str, val_rect_str)
                        img.blit(rendered_text_val_val, val_rect_val)

                    # Draw the lines for each status item
                    # WG
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX2, lineY), (lineRtX - 95, lineY), (lineRtX - 95, lineYDn)], 2)
                    # VAL
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX1, lineY), (lineRtX, lineY), (lineRtX, lineYDn)], 2)

                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    self.pageCanvas = self.pageListImg.convert()

                return self.pageCanvas, pageChanged

            # Called every view changes to this sub-page:
            def resetPage(self):
                True

        changed = True

        ######################################

        changed = True
        subPageNum = 0
        curSubPage = None

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "Misc"
            self.counter = 0

            # Set up list of sub-pages:
            MiscLine = self.MiscLine

            ### Need to add scroller

            # Loop through parameters from file and create page entry in list for each.
            for key in sorted(config.misc_itemsDIC):
                # print(config.misc_itemsDIC[key]['name'])
                # print(config.misc_itemsDIC[key]['amount'])
                # print(config.misc_itemsDIC[key]['filepath'])
                # print(config.misc_itemsDIC[key]['WG'])
                # print(config.misc_itemsDIC[key]['VAL'])
                self.currentName = str(config.misc_itemsDIC[key]['name'])
                self.currentAmount = int(config.misc_itemsDIC[key]['amount'])
                self.currentPath = pygame.image.load(config.misc_itemsDIC[key]['filepath'])
                self.currentWG = str(config.misc_itemsDIC[key]['WG'])
                self.currentVAL = str(config.misc_itemsDIC[key]['VAL'])
                if self.counter <= 0:
                    self.subPages = [
                        MiscLine(self, self.currentName, self.currentAmount, self.currentPath, self.currentWG, self.currentVAL)]
                else:
                    self.subPages.extend(
                        [MiscLine(self, self.currentName, self.currentAmount, self.currentPath, self.currentWG, self.currentVAL)])
                self.counter += 1
                # print('Counter=', self.counter)

            amounts = list(map(lambda x: '' if x < 1 else '(' + str(x) + ')',
                               (int(config.misc_itemsDIC[key]['amount']) for key in sorted(config.misc_itemsDIC))))
            """
            window_args = {"bg_color": (0, 0, 0),
                           "font": config.FONT_LRG,
                           "text_color": config.DRAWCOLOUR,
                           "text_offset": 5,
                           "sel_border_width": 2,
                           "sel_border_color": config.DRAWCOLOUR,
                           "sel_box_color": config.SELCOLOUR,
                           ### TODO store constants in config
                           "bar_rect": pygame.Rect((5, cornerPadding + cornerHeight + 4,
                                                    12, config.HEIGHT - 2 * (cornerHeight + cornerPadding) - 6)),
                           "bar_button_images": [setup.GFX["button_top"].subsurface((0, 0, 12, 6)),
                                                 setup.GFX["button_bottom"].subsurface((0, 0, 12, 6))],
                           "amounts": amounts
                           }

            bar_subrect = Subrect(
                (config.charWidth * 2.9, config.charHeight * 3, config.WIDTH * 0.35, config.HEIGHT * 0.75))
            """
            self.scroll_window = create_scrollmenu([str(config.misc_itemsDIC[key]['name']) for key in sorted(config.misc_itemsDIC)],
                                                   amounts)

            # Generate list-image for each sub-page:
            for thisPageNum in range(0, len(self.subPages)):

                thisPage = self.subPages[thisPageNum]
                thisPage.pageListImg = pygame.Surface((config.WIDTH, config.HEIGHT))

                # Do initial page-draw, caching for later use:
                thisPage.drawPage()

        def drawPage(self):
            # Pass sub-page canvas up to tab-draw function...
            subPageCanvas, subPageChanged = self.curSubPage.drawPage()
            scrollUpdate = self.scroll_window.update(subPageCanvas)

            pageChanged = (self.changed or subPageChanged or scrollUpdate)
            self.changed = False

            return subPageCanvas, pageChanged

        # Called every time view changes to this page:
        def resetPage(self):
            self.scroll_window.index = 0
            self.scroll_window.highlight_index = 0
            self.subPageNum = 0
            self.curSubPage = self.subPages[self.subPageNum]

        # Consume events passed to this page:
        def ctrlEvents(self, events):

            for event in events:
                self.scroll_window.get_events(event)
                newPageNum = self.scroll_window.highlight_index
                if (newPageNum != self.subPageNum):
                    if config.USE_SOUND:
                        config.SOUNDS["changemode"].play()
                    self.subPageNum = newPageNum
                    self.curSubPage = self.subPages[newPageNum]
                    self.changed = True

                    # Pass events to subpage too, just in case I've set them up as consumers too:
                    # self.curSubPage.ctrlEvents(events)

    class Mode_Ammo:

        # Used for line-based stats:
        class AmmoLine:

            changed = True
            firstDraw = True

            frameNum = 0

            def __init__(self, *args, **kwargs):
                self.parent = args[0]
                self.rootParent = self.parent.rootParent
                self.name = args[1]
                self.amount = args[2]
                self.pic = args[3]
                self.wg = args[4]
                self.val = args[5]
                self.efct = args[6]
                self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

            def drawPage(self):

                # Draw initial line-gauge:
                if (self.firstDraw):

                    self.firstDraw = False
                    img = self.pageListImg

                    # Assign current aid picture to variable
                    currentPic = self.pic

                    # Center the image of the Aid above the text for it
                    img.blit(currentPic, ((config.WIDTH * 0.63), (config.HEIGHT * 0.1)))

                    cornerPadding = headFoot.cornerPadding
                    charWidth = config.charWidth
                    charHeight = config.charHeight

                    # Height on page
                    lineY = (config.HEIGHT * 0.57)
                    # Line Y Down
                    lineYDn = (lineY + (charHeight * 1.6))
                    # Line from right to left
                    lineDnAX = (config.WIDTH * 0.40)
                    # Line to right hand side minus padding
                    lineRtX = (config.WIDTH - cornerPadding)

                    lineDnAX1 = (config.WIDTH * 0.80)
                    lineDnAX2 = (config.WIDTH * 0.60)
                    lineDnAX3 = (config.WIDTH * 0.40)

                    # Setup text box for word wrapping
                    textBoxWidth = (lineRtX - (lineDnAX + 6))
                    textBoxWidth1 = 21  # Each char is 7 pixels in med font * 3 char
                    textBoxWidth2 = 14
                    textBoxWidth3 = 49
                    textBoxWidth4 = 28
                    textBoxWidth5 = 50
                    textBoxWidth6 = 84
                    textBoxHeight = (charHeight * 1)

                    # Draw text box wrapped text
                    my_font = config.FONT_MED

                    ### TODO - Maybe we want ammo effects?
                    # Set label for the different status attributes
                    wg_str = "WG"
                    val_str = "VAL"
                    efct_str = "EFFECTS"

                    # Setup values for different status attributes
                    wg_value = self.wg
                    val_value = self.val
                    if isinstance(self.efct, list):
                        efct_value = ', '.join(self.efct)
                    else:
                        efct_value = self.efct
                    #print("EffectValue = " + efct_value)

                    # Create the text boxes for each attribute string
                    wg_rect_str = pygame.Rect(lineDnAX2, lineY + 4, textBoxWidth2, textBoxHeight)
                    val_rect_str = pygame.Rect(lineDnAX1, lineY + 4, textBoxWidth1, textBoxHeight)
                    efct_rect_str = pygame.Rect(lineDnAX2, lineY + 44, textBoxWidth3, textBoxHeight)

                    # Find length of attribute value so we can right align
                    wgValLength = 77 - ((len(wg_value) * 7) + 2)
                    valValLength = 77 - ((len(val_value) * 7) + 2)
                    efctValLength = 178 - ((len(efct_value) * 7) + 2)

                    # Create the text boxes for each attribute value
                    wg_rect_val = pygame.Rect(lineDnAX2 + wgValLength, lineY + 4, textBoxWidth5, textBoxHeight)
                    val_rect_val = pygame.Rect(lineDnAX1 + valValLength, lineY + 4, textBoxWidth5, textBoxHeight)
                    efct_rect_val = pygame.Rect(lineDnAX1, lineY + 44, 84, textBoxHeight * 4)

                    # Render the attribute Lables
                    rendered_text_wg_str = textrect.render_textrect(wg_str, my_font, wg_rect_str, config.DRAWCOLOUR,
                                                                    (0, 0, 0), 0)
                    rendered_text_val_str = textrect.render_textrect(val_str, my_font, val_rect_str, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)
                    rendered_text_efct_str = textrect.render_textrect(efct_str, my_font, efct_rect_str, config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)

                    # Render the attribute values
                    rendered_text_wg_val = textrect.render_textrect(wg_value, my_font, wg_rect_val, config.DRAWCOLOUR,
                                                                    (0, 0, 0), 0)
                    rendered_text_val_val = textrect.render_textrect(val_value, my_font, val_rect_val,
                                                                     config.DRAWCOLOUR,
                                                                     (0, 0, 0), 0)
                    rendered_text_efct_val = textrect.render_textrect(efct_value, my_font, efct_rect_val,
                                                                      config.DRAWCOLOUR,
                                                                      (0, 0, 0), 0)

                    # Blit text to screen
                    if (rendered_text_wg_str and rendered_text_wg_val):
                        img.blit(rendered_text_wg_str, wg_rect_str)
                        img.blit(rendered_text_wg_val, wg_rect_val)
                    if (rendered_text_val_str and rendered_text_val_val):
                        img.blit(rendered_text_val_str, val_rect_str)
                        img.blit(rendered_text_val_val, val_rect_val)
                    if (rendered_text_efct_str and rendered_text_efct_val):
                        img.blit(rendered_text_efct_str, efct_rect_str)
                        img.blit(rendered_text_efct_val, efct_rect_val)

                    # Draw the lines for each status item
                    # WG
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX2, lineY), (lineRtX - 95, lineY), (lineRtX - 95, lineYDn)], 2)
                    # VAL
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX1, lineY), (lineRtX, lineY), (lineRtX, lineYDn)], 2)
                    # EFCT CELL
                    pygame.draw.lines(img, config.DRAWCOLOUR, False,
                                      [(lineDnAX2, lineY + 40), (lineRtX, lineY + 40), (lineRtX, lineYDn + 40)], 2)

                pageChanged = self.changed
                self.changed = False

                if (pageChanged):
                    self.pageCanvas = self.pageListImg.convert()

                return self.pageCanvas, pageChanged

            # Called every view changes to this sub-page:
            def resetPage(self):
                True

        changed = True

        ######################################

        changed = True
        subPageNum = 0
        curSubPage = None

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "Ammo"
            self.counter = 0

            # Set up list of sub-pages:
            AmmoLine = self.AmmoLine

            ### Need to add scroller

            # Loop through parameters from file and create page entry in list for each.
            for key in sorted(config.ammoDIC):
                # print(config.ammoDIC[key]['name'])
                # print(config.ammoDIC[key]['amount'])
                # print(config.ammoDIC[key]['filepath'])
                # print(config.ammoDIC[key]['WG'])
                # print(config.ammoDIC[key]['VAL'])
                # print(config.ammoDIC[key]['EFFECTS'])
                self.currentName = str(config.ammoDIC[key]['name'])
                self.currentAmount = int(config.ammoDIC[key]['amount'])
                self.currentPath = pygame.image.load(config.ammoDIC[key]['filepath'])
                self.currentWG = str(config.ammoDIC[key]['WG'])
                self.currentVAL = str(config.ammoDIC[key]['VAL'])
                self.currentEFCT = config.ammoDIC[key]['EFFECTS']
                if self.counter <= 0:
                    self.subPages = [
                        AmmoLine(self, self.currentName, self.currentAmount, self.currentPath, self.currentWG,
                                 self.currentVAL, self.currentEFCT)]
                else:
                    self.subPages.extend(
                        [AmmoLine(self, self.currentName, self.currentAmount, self.currentPath, self.currentWG,
                                  self.currentVAL, self.currentEFCT)])
                self.counter += 1
                # print('Counter=', self.counter)

            amounts = list(map(lambda x: '' if x < 1 else '(' + str(x) + ')',
                               (int(config.ammoDIC[key]['amount']) for key in sorted(config.ammoDIC))))
            """
            window_args = {"bg_color": (0, 0, 0),
                           "font": config.FONT_LRG,
                           "text_color": config.DRAWCOLOUR,
                           "text_offset": 5,
                           "sel_border_width": 2,
                           "sel_border_color": config.DRAWCOLOUR,
                           "sel_box_color": config.SELCOLOUR,
                           ### TODO store constants in config
                           "bar_rect": pygame.Rect((5, cornerPadding + cornerHeight + 4,
                                                    12, config.HEIGHT - 2 * (cornerHeight + cornerPadding) - 6)),
                           "bar_button_images": [setup.GFX["button_top"].subsurface((0, 0, 12, 6)),
                                                 setup.GFX["button_bottom"].subsurface((0, 0, 12, 6))],
                           "amounts": amounts
                           }

            bar_subrect = Subrect(
                (config.charWidth * 2.9, config.charHeight * 3, config.WIDTH * 0.35, config.HEIGHT * 0.75))
            """
            self.scroll_window = create_scrollmenu(
                [str(config.ammoDIC[key]['name']) for key in sorted(config.ammoDIC)], amounts)

            # Generate list-image for each sub-page:
            for thisPageNum in range(0, len(self.subPages)):
                thisPage = self.subPages[thisPageNum]
                thisPage.pageListImg = pygame.Surface((config.WIDTH, config.HEIGHT))

                 # Do initial page-draw, caching for later use:
                thisPage.drawPage()

        def drawPage(self):
            # Pass sub-page canvas up to tab-draw function...
            subPageCanvas, subPageChanged = self.curSubPage.drawPage()
            scrollUpdate = self.scroll_window.update(subPageCanvas)

            pageChanged = (self.changed or subPageChanged or scrollUpdate)
            self.changed = False

            return subPageCanvas, pageChanged

        # Called every time view changes to this page:

        def resetPage(self):
            self.scroll_window.index = 0
            self.scroll_window.highlight_index = 0
            self.subPageNum = 0
            self.curSubPage = self.subPages[self.subPageNum]

        # Consume events passed to this page:

        def ctrlEvents(self, events):

            for event in events:
                self.scroll_window.get_events(event)
                newPageNum = self.scroll_window.highlight_index
                if (newPageNum != self.subPageNum):
                    if config.USE_SOUND:
                        config.SOUNDS["changemode"].play()
                    self.subPageNum = newPageNum
                    self.curSubPage = self.subPages[newPageNum]
                    self.changed = True

                   # Pass events to subpage too, just in case I've set them up as consumers too:
                   # self.curSubPage.ctrlEvents(events)

    def getHeaderText(self):
        return [self.name, "LVL %s" % (config.PLAYERLEVEL), "HP %s/%s" % (config.CURRHP, config.MAXHP),
                "AP %s/%s" % (config.CURRAP, config.MAXAP), "XP %s/%s" % (config.CURRXP, config.MAXXP), ]

    # Trigger page-functions
    def drawPage(self, modeNum):
        pageCanvas, pageChanged = self.modes[modeNum].drawPage()
        return pageCanvas, pageChanged

    def resetPage(self, modeNum):
        self.modes[modeNum].resetPage()

    def ctrlEvents(self, pageEvents, modeNum):
        self.modes[modeNum].ctrlEvents(pageEvents)

    # Tab init:
    def __init__(self, *args, **kwargs):
        self.parent = args[0]
        self.rootParent = self.parent.rootParent
        self.canvas = pygame.Surface((config.WIDTH, config.HEIGHT))
        self.drawnPageNum = -1

        self.modes = [self.Mode_Weapons(self), self.Mode_Apparel(self), self.Mode_Aid(self), self.Mode_Misc(self),
                      self.Mode_Ammo(self)]


        self.modeNames = ["", "", "", "", ""]
        for n in range(0, 5):
            self.modeNames[n] = self.modes[n].name


        self.header = headFoot.Header(self)

        # Generate footers for mode-pages:
        self.footerImgs = headFoot.genFooterImgs(self.modeNames)