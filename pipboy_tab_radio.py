# RasPipBoy: A Pip-Boy 3000 implementation for Raspberry Pi
#	Neal D Corbett, 2013
# 'Radio' page

import pygame, os
import config
from random import choice, randint
from pipboy_scrollmenu import create_scrollmenu
from lib import vlc
import math
from pipboy_tab import PipBoyTab

def SongFinished(event):
    myEvent = pygame.event.Event(config.EVENTS['SONG_END'], {})
    pygame.event.post(myEvent)

#https://github.com/sabas1080/pypboy
class RadioStation(object):
    STATES = {
        'stopped': 0,
        'playing': 1,
        'paused' : 2
    }

    def __init__(self, name, directory):
        """
        Radio station object
        :param name: name of the station
        :param directory: directory from which audio files are obtained
        """
        self.name = name
        self.state = self.STATES['stopped']
        self.player = vlc.MediaPlayer()
        events = self.player.event_manager()
        events.event_attach(vlc.EventType.MediaPlayerEndReached,
                            SongFinished)
        self.filename = None
        if directory is not None:
            self.files = self.load_files(directory)

    def choose_random(self):
        """
        Initializes next random song
        :return: None
        """
        if len(self.files) > 1:
            f = choice(list(filter(lambda x: x != self.filename, self.files)))
            self.filename = f
        else:
            self.filename = self.files[0]

    def play_random(self):
        """
        Starts playing the next random song
        :return: None
        """
        self.choose_random()
        media = vlc.Media(self.filename)
        self.player.set_media(media)
        self.player.play()
        self.state = self.STATES['playing']

    def play(self):
        """
        Resumes playback if was paused, or start a new random song if stopped
        :return: None
        """
        if self.state == self.STATES['paused']:
            self.player.play()
            self.state = self.STATES['playing']
        elif self.state == self.STATES['stopped']:
            self.play_random()

    def pause(self):
        """
        Pauses playback
        :return: None
        """
        self.state = self.STATES['paused']
        self.player.pause()

    def stop(self):
        """
        Stops playback
        :return:
        """
        self.state = self.STATES['stopped']
        self.player.stop()

    def load_files(self, directory):
        """
        Loads all audio file names from the given directory
        :param directory: path to audio files
        :return: list of audio file in this directory
        """
        files = []
        for f in os.listdir(directory):
            if f.endswith(".mp3") or f.endswith(".ogg") or f.endswith(".wav"):
                files.append(os.path.join(directory, f))
        return files


class DummyRadioStation(RadioStation):
    """
    A dummy radio station, works as a pause button in station list
    """
    def __init__(self):
        super(DummyRadioStation, self).__init__('PAUSE', None)

    def play(self):
        pass

    def pause(self):
        pass

    def stop(self):
        pass

    def play_random(self):
        pass


class Oscilloscope(object):
    def __init__(self, rect, line_thickness=2, small_notch_size=5, big_notch_size=10,
                 speed=math.pi/2, min_duration=3, max_duration=7):
        """
        Oscilloscope is a GUI element which has axes and animation representing sound waves
        :param rect: pygame.Rect, area where the oscilloscope will be drawn
        :param line_thickness: int, thickness of all lines in pixels
        :param small_notch_size: int, length of small notcehs in pixels
        :param big_notch_size:  int, length of big notches in pixels
        :param speed: double, how many waves per second will pass in the oscilloscope
        :param min_duration: int, minimum number of waves that will pass before change
        :param max_duration: int, maximum number of waves that will pass before change
        """
        height = rect.height
        width = rect.width
        self.rect = rect
        self.line_thickness = line_thickness
        self.canvas = pygame.Surface((width, height))

        # Drawing rulers
        point_list = [(line_thickness, height-line_thickness),
                      (width-line_thickness, height-line_thickness),
                      (width-line_thickness, line_thickness)]
        pygame.draw.lines(self.canvas, config.DRAWCOLOUR, False, point_list, line_thickness)

        i = 0
        for x in range(line_thickness*2, width-2*line_thickness, line_thickness*2):
            if i == 5:
                i = 0
                notch_size = big_notch_size
            else:
                i += 1
                notch_size = small_notch_size
            pygame.draw.line(self.canvas, config.DRAWCOLOUR,
                             (x, height-line_thickness),
                             (x, height-line_thickness-notch_size))

        i = 0
        for y in range(line_thickness * 2, height - 2*line_thickness, line_thickness * 2):
            if i == 5:
                i = 0
                notch_size = big_notch_size
            else:
                i += 1
                notch_size = small_notch_size
            pygame.draw.line(self.canvas, config.DRAWCOLOUR,
                             (width - line_thickness, y),
                             (width - line_thickness - notch_size, y))

        # Initializing radio waves
        self.running = False
        self.waves = []
        for wave in ['wave1', 'wave2', 'wave3']:
            self.waves += [pygame.transform.smoothscale(config.IMAGES[wave], self.canvas.get_size())]

        self.current_wave = choice(self.waves)
        self.last_wave = self.current_wave
        # Animation parameters
        # How many waves should pass by for the next one to appear
        self.duration = 0.
        self.min_duration = min_duration
        self.max_duartion = max_duration
        # Current position of the wave
        self.position = 0.
        # Speed at which the wave flies by
        self.speed = speed/config.FPS

    def next_sequence(self):
        """
        Initializes next wave after when the previous was finished
        :return: None
        """
        self.position = math.modf(self.position)[0]
        self.current_wave = choice(list(filter(lambda x: x != self.current_wave, self.waves)))
        self.duration = randint(self.min_duration, self.max_duartion)

    def start(self):
        """
        Starts the animation
        :return: None
        """
        self.next_sequence()
        self.running = True

    def stop(self):
        """
        Stops the animation
        :return: None
        """
        self.running = False

    def draw(self, canvas):
        """
        Draws the oscilloscope on screen
        :param canvas: pygame.Surface, screen canvas
        :return: None
        """
        canvas.blit(self.canvas, self.rect)

        if self.running:
            if self.last_wave != self.current_wave and self.position > 0:
                self.last_wave = self.current_wave
            offset = abs(math.trunc(math.modf(self.position+1)[0]*self.rect.width))
            targetRect = self.rect.move(-offset, 0).clip(self.rect)
            area = targetRect.copy()
            area.left = offset
            area.top = 0
            canvas.blit(self.last_wave, targetRect, area)

            targetRect = self.rect.move(self.rect.width-offset, 0).clip(self.rect)
            area = targetRect.copy()
            area.left = 0
            area.top = 0
            canvas.blit(self.current_wave, targetRect, area)

            self.position += self.speed
            if self.position >= self.duration - 1:
                self.next_sequence()



class Mode_Radio:
    changed = True

    def __init__(self, *args, **kwargs):
        self.parent = args[0]
        self.rootParent = self.parent.rootParent
        self.name = "Radio"
        self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))
        self.stations = [DummyRadioStation()]
        for station_name in config.RADIO_STATIONS:
            self.stations += [RadioStation(station_name, config.RADIO_STATIONS[station_name])]

        self.scroll_window = create_scrollmenu([x.name for x in self.stations])
        offset = 15
        self.oscilloscope = Oscilloscope(pygame.Rect(config.WIDTH // 2 + offset,
                                                     config.HEIGHT // 4,
                                                     config.WIDTH // 2 - config.cornerPadding - offset,
                                                     config.HEIGHT // 2), 2,
                                         5, 10)

    def drawPage(self):
        scrollUpdate = self.scroll_window.update(self.pageCanvas)
        pageChanged = (self.changed or scrollUpdate or self.oscilloscope.running)
        if pageChanged:
            self.oscilloscope.draw(self.pageCanvas)

        self.changed = False

        return self.pageCanvas, pageChanged

    # Called every time view changes to this page:
    def resetPage(self):
        pass

    # Consume events passed to this page:
    def ctrlEvents(self, events):
        active_station = self.scroll_window.highlight_index

        for event in events:
            if isinstance(event, pygame.event.EventType):
                if event.type == config.EVENTS['SONG_END']:
                    self.stations[self.scroll_window.highlight_index].play_random()
            else:
                self.scroll_window.get_events(event)
                new_station = self.scroll_window.highlight_index
                if new_station != active_station:
                    if config.USE_SOUND:
                        config.SOUNDS["changemode"].play()
                    self.stations[active_station].pause()
                    active_station = new_station
                    self.stations[active_station].play()
                    if active_station == 0:
                        self.oscilloscope.stop()
                    else:
                        self.oscilloscope.start()

                    self.changed = True



class Tab_Radio(PipBoyTab):
    def __init__(self, parent):
        super().__init__(
            parent=parent,
            name="RADIO",
            modes=lambda parent: [Mode_Radio(parent)]
        )